# ============================================================================
# File: test03_gain_measurement.py
# ------------------------------
#
# Notes:
#   - Adapted from 'test04_scurve_xavi.py'.
#   - Added read options from LHCb format.
#   - Running in EVT_ITOT mode
#
# Description:
#   - Returns global gain [mV/LSB]
#
# Layout:
#   configure and prepare
#   for every amp:
#       for every thr:
#           get data -> s curve or gaussian
#       for every pixel:
#           fit measured data with errorfunction or gaussian
#           extract mean, sigma -> one fit for each amp value and each pixel
#   for every pixel:
#       fit extracted means linearly
#       extract gradient
#   take mean and rms of gradient
#   calculate resolution from noise and gradient rms
#   save and finish
#
# Status:
#   Works well
#
# ============================================================================

from tpx3_test import *
from SpidrTpx3_engine import *
from scipy.special import erf
from scipy.optimize import curve_fit
from dac_defaults import dac_defaults
import os
import random
import time
import logging
import matplotlib.pyplot as plt
import numpy as np



# Definitions
# ============================================================================

def linef(x, *p):
    k, d = p
    return  np.array(x) * k + d

def line(x, *p):
    k = p
    return  np.array(x) * k


def gauss(x, *p):
    A, mu, sigma = p
    return A * np.exp(-(x-mu)**2 / (2. * sigma**2))


def errorf(x, *p):
    a, mu, sigma = p
    return 0.5 * a * (1.0+erf((x-mu)/sigma))


def errorfc(x, *p):
    a, mu, sigma = p
    return 0.5 * a * (1.0-erf((x-mu)/(sigma*1.4142)))


class sGnuplot:
    def __init__(self, fname):
        self.fout = fname
        self.fgnu = fname[:-4] + ".gnu"
        self.gnu = open(self.fgnu, "w")
        self("set output '%s'" % self.fout)

    def __call__(self, *args):
        for a in args:
          self.gnu.write(a + "\n")

    def run(self):
        self.gnu.close()
        os.system("gnuplot %s" % self.fgnu)



# Main Class
# ============================================================================

class test03_gain_measurement(tpx3_test):
    """Diagonal scurves, uses EVT_ITOT mode.

    Parameter:
    npulses ... number of testpulses sent per shutter
    period_dac ... sets duration between two testpulses, 8bit dac
    """

    def _execute(self, **keywords):
        self.tpx.resetPixels()
        self.tpx.shutterOff()
        self.tpx.setDacsDflt()

        params = {}


        # Configuration
        # -----------------------------

        ## Testpulse Configuration
        npulses = 200               # number of testpulses sent per shutter
        period_dac = 0x4            # sets duration between two testpulses, 8bit dac

        ## Main Configuration
        params['ik_dac'] = 10
        params['amps'] = [0, 1000, 3000, 6000]
        params['shutter_len'] = 15000
        params['shutter_len_noise'] = 400

        params['gen_config'] = TPX3_ACQMODE_EVT_ITOT | TPX3_TESTPULSE_ENA | TPX3_GRAYCOUNT_ENA
        params['pll_config'] = TPX3_PLL_RUN | TPX3_VCNTRL_PLL | TPX3_DUALEDGE_CLK | TPX3_PHASESHIFT_DIV_8 | TPX3_PHASESHIFT_NR_16

        ## Threshold Options
        if 'th_start' in keywords: params['th_start'] = int(keywords['th_start'])
        if 'th_stop' in keywords: params['th_stop'] = int(keywords['th_stop'])
        if 'th_step' in keywords: params['th_step'] = int(keywords['th_step'])

        if polarity:
            params['gen_config'] |= TPX3_POLARITY_EMIN
            params['shutter_len'] = params['shutter_len']
            params['shutter_len_noise'] = params['shutter_len_noise']/2
            params['th_start'] = 2000
            params['th_stop'] = 900
            params['th_step'] = -2
        else:
            params['th_start'] = 300
            params['th_stop'] = 1300
            params['th_step'] = 2



        # Preperation
        # -----------------------------

        ## Set config directory
        bname = self.tpx.readName()
        fbase = "config/%s/" % (bname)
        self.tpx.loadDACs(directory = fbase, chipname = bname)
        self.logging.info( "Chip name    : %s" % bname)

        ## Set operating parameters
        self.tpx.setGenConfig(params['gen_config'])
        self.tpx.setPllConfig(params['pll_config'])
        self.tpx.setTpPeriodPhase(period_dac, 0)
        self.tpx.setTpNumber(npulses)

        ## Set operating conditions
        self.logging.info("Optimization of DC operating point")
        self.tpx.setDac(TPX3_IBIAS_IKRUM, params['ik_dac'])
        # self.tpx.setDac(TPX3_IBIAS_DISCS1_ON, 100)
        # self.tpx.setDac(TPX3_IBIAS_DISCS2_ON, 128)
        # self.tpx.setDac(TPX3_IBIAS_PREAMP_ON, 128)
        # self.tpx.setDac(TPX3_IBIAS_PIXELDAC, 128)
        # self.tpx.setDac(TPX3_VFBK, 164)

        ## Set dac bits
        self.wiki_banner(**keywords)
        self.tpx.setDecodersEna(True)

        ## Reset Pixels
        self.tpx.resetPixelConfig()

        ## Load equalisation codes and mask in Xavi's format
        if 0:
            codes_eq = np.loadtxt(fbase + "/eq_codes_finestep_%s_f.dat" % bname, int)
            mask = np.loadtxt(fbase + "/eq_mask_finestep_%s_f.dat" % bname, int)
            self.logging.info( "Mask and trim dacs loaded from Xavi's format.")

        ## Load equalisation codes and mask in LHCb format
        if 1:
            bfullname = "%s%04i_%s%02i" % ("W", int(bname.split("_")[0].split("W")[1]), bname.split("_")[1][0], int(bname.split("_")[1][1:]))
            trim = np.loadtxt(fbase + "/%s_trimdacs.txt" % (bfullname))
            codes_eq = np.zeros((256, 256), int)
            mask = np.zeros((256, 256), int)
            for x in range(256):
                for y in range(256):
                    codes_eq[y][x] = trim[x*256+y][2]
                    mask[y][x] = trim[x*256+y][3]
            self.logging.info("Mask and trim dacs loaded from LHCb format.")

        self.logging.info("Discharge current at %d. Noise shutter length is %d us." % (params['ik_dac'], params['shutter_len_noise']))
        self.logging.info("Chip Temperature is %.1f degrees" % self.tpx.getTpix3Temp())

        ## Set equalisation and mask
        for x in range(256):
             for y in range(256):
                self.tpx.setPixelThreshold(x, y, codes_eq[y][x])
                self.tpx.setPixelMask(x, y, mask[y][x])

        ## Set ctpr bits
        self.tpx.setCtprBits(0)
        for x in range(256):
            self.tpx.setCtprBit(x, 1)
        self.tpx.setCtpr()

        ## Set log directory
        logdir = self.fname + "/details/"



        # Run
        # -----------------------------

        self.tpx.setDac(TPX3_VTP_COARSE, 64)
        self.tpx.setSenseDac(TPX3_VTP_COARSE)
        coarse = self.tpx.get_adc(32)
        fit_res = {}
        fit_res_noise = {}
        thr_volts = [0.0]


        vtp_code = []
        for amp in range(len(params['amps'])):
            self.warning_detailed_restart()
            fit_res[amp] = {} # mean of scurve in thr_dac
            fit_res_noise[amp] = {} # mean of gaussian in thr_dac
            voltage = 0
            electrons = 0

            ## Optimize test pulse amplitude
            if amp > 0:
                self.tpx.setSenseDac(TPX3_VTP_FINE)
                shutter_length = int(((2*(64*period_dac+1)*npulses)/40) + 100)
                time.sleep(0.1)
                best_vtpfine_diff = 2000
                best_vtpfine_code = 0
                target = params['amps'][amp]

                for code in range(64, 512):
                    self.tpx.setDac(TPX3_VTP_FINE, code) # (0e-) slope 44.5e/LSB -> (112=1000e-) (135=2000e-)
                    time.sleep(0.001)
                    fine = self.tpx.get_adc(64)
                    electrons = 1000.0 * 20.2 * (fine-coarse)

                    if abs(electrons - target) < abs(best_vtpfine_diff) and electrons > 0:
                        best_vtpfine_diff = electrons - target
                        best_vtpfine_code = code

                self.tpx.setDac(TPX3_VTP_FINE, best_vtpfine_code)
                print "  TPX3_VTP_FINE code=%d %.2f v_fine=%.1f v_coarse=%.1f" % (best_vtpfine_code, best_vtpfine_diff, fine*1000.0, coarse*1000.0)

                fine = self.tpx.get_adc(64)
                voltage = 1000.0 * abs(fine-coarse)
                thr_volts.append(voltage)
                vtp_code.append(best_vtpfine_code)
                electrons = 20.2 * voltage
                self.logging.info("TPX3_VTP_FINE code=%d v_fine=%.1f v_coarse=%.1f" % (best_vtpfine_code, fine*1000.0, coarse*1000.0))
                self.logging.info("Test pulse voltage %.4f mV (~ %.0f e-)" % (voltage, electrons))

            else:
                self.logging.info("Test pulse voltage 0.0 mV (0 e-)")

            ## Set mask for pixels and test pulse
            self.tpx.setPixelMask(ALL_PIXELS, ALL_PIXELS, 1)
            for col in range(256):
                if mask[col][col] == 0:
                    self.tpx.setPixelMask(col, col, 0)
                    if amp > 0:
                        self.tpx.setPixelTestEna(col, col, testbit=True)

            self.mask_bad_pixels()
            self.tpx.pauseReadout()
            self.tpx.setPixelConfig()
            self.tpx.sequentialReadout(tokens=1)
            self.tpx.resetPixels()

            ## Set shutter length
            if amp == 0:
                shutter_length = params['shutter_len_noise']
            self.tpx.setShutterLen(shutter_length)

            ## Scan through all the thresholds and record number of counts
            ## Open shutter, get frame, for every pixel read event counter and write to res
            res = {}
            res_itot = {}

            for threshold in range(params['th_start'], params['th_stop']+1, params['th_step']):
                self.tpx.setThreshold(threshold)
                self.tpx.openShutter()
                data = self.tpx.get_frame()

                for d in data:
                    if d.type == 0xA:
                        if d.col == d.row:
                            if not d.col in res:
                                res[d.col] = {}
                                res_itot[d.col] = {}
                            if not d.row in res[d.col]:
                                res[d.col][d.row] = {}
                                res_itot[d.col][d.row] = {}
                            res[d.col][d.row][threshold] = d.event_counter
                            res_itot[d.col][d.row][threshold] = d.event_counter

                        elif not (d.col, d.row) in self.bad_pixels: # suppress information about the pixels which are known to be bad/noisy/...
                            self.warning_detailed("Unexpected packet %s" % str(d))

                    elif d.type!=0x7:
                        self.warning_detailed("Unexpected packet %s" % str(d))

            self.warning_detailed_summary()

            ## Write to file and fit flags
            w2f = True
            fit = True

            ## Fit and create plots
            if w2f:
                ## Prepare gnuplot
                dn = logdir + "amp%02d/" % (amp)
                self.logging.info("Saving files to %s" % dn)
                self.mkdir(dn)

                g = sGnuplot(dn + "plot.png")
                g("set terminal png size 800,600", "set grid", "set xtic 128",
                "set xr [0:2000]", "set yr [0:1024]", "set ytic 128",
                "set xlabel 'Threshold[LSB]'", "set ylabel 'Counts'")
                pcmd = 'plot '

                for col in range(256):
                    row = col
                    if (col, row) in self.bad_pixels:
                        continue
                    if not col in res or not row in res[col]:
                        self.logging.warning("No data for pixel (%d,%d)" % (col,row))
                        continue

                    ## Fit
                    if fit:
                        ## Fit noise scan with Gaussian
                        if amp == 0:
                            codes = [] # in thr_dac
                            vals = [] # in pc
                            avr = 0.0
                            N = 0
                            for code in sorted(res[col][row]):
                                val = res[col][row][code]
                                if val > 2:
                                    codes.append(code)
                                    vals.append(val)
                                    avr += code * val
                                    N += val
                            fit_res[amp][col] = -1.0
                            fit_res_noise[amp][col] = -1.0
                            if N > 2:
                                avr = avr/N
                                try:
                                    p0 = [max(vals), avr, 6.]
                                    coeff, var_matrix = curve_fit(gauss, codes, vals, p0=p0)
                                    fit_res[amp][col] = coeff[1]
                                    fit_res_noise[amp][col] = coeff[2]
                                except:
                                    pass
                            else:
                                self.logging.warning("No hits for pixel (%d, %d)" % (col,row))

                        ## Fit testpulse scan with error function
                        else:
                            codes = []
                            vals = []
                            avr = 0.0
                            N = 0
                            for code in sorted(res[col][row]):
                                if code < fit_res[0][col] and polarity:
                                    continue # holes
                                if code > fit_res[0][col] and not polarity:
                                    continue # electrons
                                val = res[col][row][code]
                                if val < (npulses + 2):
                                    codes.append(code)
                                    vals.append(val)
                                    avr += code * val
                                    N += val
                            fit_res[amp][col] = -1.0
                            fit_res_noise[amp][col] = -1.0
                            if N > 2:
                                avr = avr/N
                                try:
                                    if 1:
                                        if not polarity:
                                            p0 = [npulses, min(codes)+5, 6.]
                                            coeff, var_matrix = curve_fit(errorf, codes, vals, p0=p0)
                                        else:
                                            p0 = [npulses, max(codes)-25, 6.]
                                            coeff, var_matrix = curve_fit(errorfc, codes, vals, p0=p0)
                                        fit_res[amp][col] = coeff[1]
                                        fit_res_noise[amp][col] = coeff[2]
                                        # f = open("/tmp/f%d.dat" % col, "w")
                                        # for c in range(len(codes)):
                                        #     fit = errorf(codes[c], *coeff)
                                        #     f.write("%d %d %d\n" % (codes[c], vals[c], fit))
                                        # f.close()
                                except:
                                    pass
                            else:
                                self.logging.warning("No hits for pixel (%d,%d)" % (col,row))

                    ## Write to file
                    if w2f:
                        fn = dn + "/%03d_%03d.dat" % (col, row)
                        f = open(fn, "w")
                        f.write("# amp step %d TPX3_VTP_FINE %d\n" % (amp, params['amps'][amp]))
                        f.write("# voltage  %.4f (measured)\n" % (voltage))
                        f.write("# charge   %.1f (estimated)\n" % (electrons))
                        f.write("# THR:%.2f\tENC:%.2f\n" % (coeff[1], coeff[2]))
                        for code in sorted(res[col][row]):
                             f.write("%d %d\n" % (code,res[col][row][code]))
                        f.close()
                        if len(pcmd) > 5:
                            pcmd += ','
                        pcmd += "'%s' w l t '' " % fn

            ## Execute gnuplot
            g(pcmd)
            g.run()
            self.logging.info("Saving plot to %s" % g.fout)




        # Finishing
        # -----------------------------

        gains = []
        baselines = []

        ## Calcluate gain with linear fit over the 4 mean values
        ## Allow offset
        if 0:
            for col in range(256):
                thr_dacs = []

                for amp in range(0, len(params['amps'])):
                    if not col in fit_res[0] or fit_res[0][col] < 0.0:
                        if amp == 0:
                            self.logging.info("No baseline point for pixel (%d, %d)" % (col, col))
                            continue
                        else:
                            self.logging.info("No amplitude %d point for pixel (%d, %d)" % (amp, col, col))
                            continue
                    thr_dacs.append(fit_res[amp][col])

                if polarity:
                    for voltage in thr_volts:
                        voltage = -voltage

                if len(thr_dacs) != len(thr_volts):
                    continue

                p0 = [thr_volts[-1]/thr_dacs[-1], 1000]
                coeff, var_matrix = curve_fit(linef, thr_dacs, thr_volts, p0=p0)
                gains.append(coeff[0])
                baselines.append(-coeff[1]/coeff[0])

        ## Allow no offset
        if 1:
            for col in range(256):
                if not col in fit_res[0] or fit_res[0][col] < 0:
                   self.logging.info("No baseline point for pixel (%d, %d)" % (col, col))
                   continue

                thr_dacs = [0.0]

                for amp in range(1, len(params['amps'])):
                    if not col in fit_res[0] or fit_res[0][col] < 0.0:
                        self.logging.info("No amplitude %d point for pixel (%d, %d)" % (amp, col, col))
                        continue
                    thr_dacs.append(-fit_res[amp][col] + fit_res[0][col])

                if len(thr_dacs) != len(thr_volts):
                    continue

                p0 = [thr_volts[-1]/thr_dacs[-1]]
                coeff, var_matrix = curve_fit(line, thr_dacs, thr_volts, p0=p0)
                gains.append(coeff[0])
                baselines.append(fit_res[0][col])

        ## Various calculations and logging
        gmean = np.mean(gains)
        grms = np.std(gains)
        bmean = np.mean(baselines)
        brms = np.std(baselines)

        proc = 100.0 * grms/gmean
        electrons = 20.2 * gmean
        temp = self.tpx.getTpix3Temp()
        self.logging.info("")
        self.results['GAIN_ELECTRONS'] = "%.2f" % electrons
        self.results['GAIN_MEAN'] = "%.2f" % gmean
        self.results['GAIN_RMS'] = "%.3f" % grms
        self.results['GAIN_SPREAD'] = "%.1f" % proc
        self.logging.info("Gain mean %.3f +/- %.3f mV/LSB (~%.1f%%)  [~%.2f e- / TH LSB]  %.2f C" % (gmean, grms, proc, electrons, temp))
        self.logging.info("Baseline mean %.2f +/- %.2f LSB" % (bmean, brms))

        self.tpx.setDac(TPX3_VTP_COARSE, 64)
        self.tpx.setSenseDac(TPX3_VTP_COARSE)
        coarse = self.tpx.get_adc(32)

        ## Calculate resolution
        for amp in range(len(params['amps'])):
            thr = []
            noise = []
            for col in range(256):
                if mask[col][col] == 0:
                    thr.append(fit_res[amp][col])
                    noise.append(fit_res_noise[amp][col])

            mean_thr = np.mean(thr)
            mean_thr_rms_e = np.std(thr) * electrons
            mean_noise_e = np.mean(noise) * electrons
            mean_noise_rms_e = np.std(noise) * electrons
            energy_resolution = pow(pow(mean_thr_rms_e, 2) + pow(mean_noise_e, 2), 0.5)
            dac_code_ikrum = self.tpx.getDac(TPX3_IBIAS_IKRUM)
            self.tpx.setDac(TPX3_VTP_FINE,vtp_code[amp - 1]) # re-mesure TP setting
            self.tpx.setSenseDac(TPX3_VTP_FINE)
            fine = self.tpx.get_adc(32)
            if amp == 0:
                fine = coarse
                baseline = np.mean(thr)
                baseline_rms = np.std(thr)
                mean_noise = np.mean(noise)

                if polarity:
                    thr_op = bl_mean + 6 * pow(pow(baseline_rms, 2) + pow(mean_noise, 2), 0.5)
                else:
                    thr_op = bl_mean - 6 * pow(pow(baseline_rms, 2) + pow(mean_noise, 2), 0.5)

            self.logging.info("Ik:%d\tPolarity %d\t Gain[%d]:%.1f\t(%3.2f)e-\t THR:%.1f\t +/-%.1fe-\t NOISE:%.1fe-\t +/-%.1fe-\t Resolution:%.1f (pix: %d,%d)"\
                % (dac_code_ikrum, polarity, amp, thr_volts[amp], 1000.0 * 20 * (fine-coarse), mean_thr, mean_thr_rms_e, mean_noise_e, mean_noise_rms_e, energy_resolution, len(thr), len(noise)))

        self.logging.info("")
        self.logging.info("Recommended operational threshold: %d (6 sigmas away from baseline)", thr_op)
        self.logging.info("")
