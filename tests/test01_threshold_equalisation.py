# ============================================================================
# File: test01_threshold_equalisation.py
# ------------------------------
#
# Notes:
#   - Adapted from 'test08_THL_equalisation_nalipour'.
#
# Layout:
#   configure and prepare
#   for 2 cdacs:
#       make threshold scan over noise floor or testpulse
#       get threshold matrix
#       calculate mean and std of threshold distribution at this cdac
#   calculate target as mid point between the two means
#   [rough equalisation]
#   for each pixel:
#       find cdac that is closest to the target value
#       set this cdac
#       make threshold scan over noise floor or testpulse
#   [fine equalisation]
#   for each pixel:
#       check if ideal cdac has changed
#       if yes, set new ideal value
#       make threshold scan over noise floor or testpulse
#   repeat fine equalisation several times
#   save and finish
#
#
# Status:
#   Columns and rows are kinda messy but everything should be fine now
#
# ============================================================================


from tpx3_test import *
from SpidrTpx3_engine import *
from dac_defaults import dac_defaults
from scipy.optimize import curve_fit
import os
import random
import time
import logging
import shutil
import sys
import socket
import getpass
import numpy as np
import matplotlib.pyplot as plt

# for writing xml file
from xml.etree import ElementTree
from xml.dom import minidom
from xml.etree.ElementTree import Element, SubElement, Comment




# Definitions
# ============================================================================

def gauss(x, *p):
    A, mu, sigma = p
    return A * np.exp(-(x - mu)**2 / (2. * sigma**2))


def get_date_time():
    return time.strftime("%Y/%m/%d %H:%M:%S", time.localtime())


def get_user_name():
    return getpass.getuser()


def get_host_name():
    return socket.gethostname()


def prettify(elem):
    """
    Return a pretty-printed XML string for the Element.
    """
    rough_string = ElementTree.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent = "  ")


def _matrixToStr(m):
    m_str = ""
    for x in range(len(m)):
        for y in range(len(m)):
            m_str += "%d " % m[x][y]
        m_str += "\n"
    return m_str




# Main Class
# ============================================================================

class test01_threshold_equalisation(tpx3_test):
    """Threshold equalisation with noise or testpulses.

    Parameters:
    polarity ... polarity flag, holes = 0, electrons = 1
    testpulses ... testpulse flag, equalise at baseline = 0, with testpulse = 1
    update ... update dacs, trim and mask files in the config folder automatically

    ik_dac ... discharge current dac
    pix_dac ... trim bit dynamic range dac
    period_dac ... sets duration between two testpulses, only relevant if testpulses = 1

    shutter_length ... frame length in seconds

    npulses ... number of testpulses to be sent, only relevant if testpulses = 1
    spacing ... mask spacing, only relevant if testpulses = 1
    target ... voltage to equalise at, only relevant if testpulses = 1
    """


    # LHCb configuration
    # ---------------------------------

    def LHCb_configStyle(self, bname, codes, masks, coarseVal, fineVal):

        ## Write general DACs
        f = open(bname + "_dacs.txt", "w")
        all_dacs=["TPX3_IBIAS_PREAMP_ON", "TPX3_IBIAS_PREAMP_OFF", "TPX3_VPREAMP_NCAS", "TPX3_IBIAS_IKRUM", "TPX3_VFBK",\
                  "TPX3_VTHRESH_FINE", "TPX3_VTHRESH_COARSE", "TPX3_IBIAS_DISCS1_ON", "TPX3_IBIAS_DISCS1_OFF",\
                  "TPX3_IBIAS_DISCS2_ON", "TPX3_IBIAS_DISCS2_OFF", "TPX3_IBIAS_PIXELDAC", "TPX3_IBIAS_TPBUFIN",\
                  "TPX3_IBIAS_TPBUFOUT", "TPX3_VTP_COARSE", "TPX3_VTP_FINE", "TPX3_IBIAS_CP_PLL", "TPX3_PLL_VCNTRL"]
        bits_perDac=["[0-255]", "[0-15]", "[0-255]", "[0-255]", "[0-255]", "[0-512]", "[0-15]", "[0-255]", "[0-15]", "[0-255]", "[0-15]", "[0-255]", "[0-255]", "[0-255]", "[0-255]", "[0-512]", "[0-255]", "[0-255]"]

        f.write("# TPX3 DAC settings\n")
        f.write("# lines starting with # are skipped\n")
        f.write("#\n")
        f.write("# reg_nr reg_value \n")
        f.write("#  \n")
        for i in range(0, len(all_dacs)):
            dac = all_dacs[i]
            str_to_write = "\t%i" % (i+1)
            str_to_write += "\t%d" % self.tpx.getDac(eval(dac))
            # if (dac != "TPX3_VTHRESH_FINE" or dac != "TPX3_VTHRESH_COARSE"):
            #     str_to_write += "\t%d" % self.tpx.getDac(eval(dac))
            # elif (dac == "TPX3_VTHRESH_FINE"):
            #     str_to_write += "\t%d"%fineVal
            # elif (dac == "TPX3_VTHRESH_COARSE"):
            #     str_to_write += "\t%d"%coarseVal

            str_to_write += "\t# "
            str_to_write += "%s\t" % dac
            str_to_write += "%s\t\n" % bits_perDac[i]
            f.write(str_to_write)

        f.close()

        ## Write trim DACs
        fTrims = open(bname + "_trimdacs.txt", "w")
        fTrims.write("#col row trim mask tp_ena \n")

        for col in range(0, 256):
            for row in range(0, 256):
                fTrims.write("%i\t%i\t%i\t%i\t0\t \n" % (col, row, codes[row][col], masks[row][col]))

        fTrims.close()



    # Save Config Files
    # ---------------------------------

    def saveConfiguration(self, fname, codes, masks, vthresh_value):
        print("Save config to %s"%fname)
        root = Element("Timepix3")
        info = SubElement(root, "info")
        time_now = SubElement(info, "time")
        time_now.set("time", get_date_time())
        user = SubElement(info, "user")
        user.set("user", get_user_name())
        host = SubElement(info, "host")
        host.set("host", get_host_name())

        regs = SubElement(root, "registers")
        all_dacs=["TPX3_IBIAS_PREAMP_ON", "TPX3_IBIAS_PREAMP_OFF", "TPX3_VPREAMP_NCAS", "TPX3_IBIAS_IKRUM", "TPX3_VFBK",\
                "TPX3_VTHRESH", "TPX3_IBIAS_DISCS1_ON", "TPX3_IBIAS_DISCS1_OFF",\
                "TPX3_IBIAS_DISCS2_ON", "TPX3_IBIAS_DISCS2_OFF", "TPX3_IBIAS_PIXELDAC", "TPX3_IBIAS_TPBUFIN",\
                "TPX3_IBIAS_TPBUFOUT", "TPX3_VTP_COARSE", "TPX3_VTP_FINE"]
        for dac in all_dacs:
            reg = SubElement(regs, "reg")
            reg.set("name", dac[5:])
            if dac!="TPX3_VTHRESH":
                reg.set("value", "%d" % self.tpx.getDac(eval(dac)))
            else:
                reg.set("value", "%d" % vthresh_value)

        reg = SubElement(regs, "reg")
        reg.set("name", "GeneralConfig")
        reg.set("value", "0x%08x"%self.tpx.getGenConfig())

        reg = SubElement(regs, "reg")
        reg.set("name", "PllConfig")
        reg.set("value", "0x%08x"%self.tpx.getPllConfig())

        reg = SubElement(regs, "reg")
        r, val = self.tpx.ctrl.getOutBlockConfig(self.tpx.id)
        self.tpx._log_ctrl_cmd("getOutBlockConfig()=%02x" % (val), r)
        reg.set("name", "OutputBlockConfig")
        reg.set("value", "0x%08x"%val)

        codes_se = SubElement(root, "codes")
        mask_se  = SubElement(root, "mask")

        codes_se.text = _matrixToStr(codes)
        mask_se.text = _matrixToStr(masks)

        f = open(fname, "w")
        f.write(prettify(root))
        f.close()




    # Threshold Scan Edge
    # ---------------------------------

    def threshold_scan_edge(self, res=None, fromThr=0, toThr=512, threshold=10, step=1):
        peak = np.zeros((256, 256), int)

        ## set up data vector
        if res == None:
            res = {}
            for x in range(256):
                res[x] = {}
                for y in range(256):
                    res[x][y] = 0

        mask = 0
        anim = ['|','/','-','\\','|','/','-','\\']

        ## for ever every threshold, open shutter for a given time and record how many events there are per pixel
        for i in range(fromThr, toThr, step):
            self.tpx.setThreshold(i)

            if 0 and len(data) > 1:
                for d in data:
                    logging.warning("after setdac %s" % (str(d)))

            self.tpx.openShutter()
            data = self.tpx.get_frame()

            ## process data
            for d in data:
                if d.type == 0xA or d.type == 0xB:

                    ## if ncnt above a certain number of cnts, record the thr_dac when it happens
                    ## for that pixel and mask it for the rest of the run so that it is not overwritten
                    if  d.event_counter >= threshold and res[d.col][d.row] == 0:
                        res[d.col][d.row] = i
                        self.tpx.setPixelMask(d.col, d.row, 1)
                        mask += 1

                elif d.type != 0x7:
                    logging.warning("Unexpected packet %s" % str(d))

            ## print info if too many masked pixels
            if mask > 7500:
                logging.debug("Masking %d pixels" % mask)
                self.tpx.pauseReadout()
                self.tpx.setPixelConfig()
                self.tpx.sequentialReadout(tokens=4)

                mask = 0

            ## print some info
            if 1:
                print "%c %s %d/%d (packets %d)" % (13, anim[i%len(anim)], i, toThr, len(data)),
                if i == toThr-1: print
                sys.stdout.flush()

        ## return a matrix that contains the thr_dac at which the pixel crosses the given threshold value
        return res




    # Threshold Scan Peak
    # ---------------------------------

    def threshold_scan_peak(self, res=None, fromThr=1000, toThr=1300, step=5):
        peak = np.zeros((256,256), int)

        if res == None:
            res = {}
            for x in range(256):
                res[x] = {}
                for y in range(256):
                    res[x][y]=0

        pre_thr = {}
        diff = {}
        for x in range(256):
            pre_thr[x] = {}
            diff[x] = {}
            for y in range(256):
                pre_thr[x][y] = 0
                diff[x][y] = 0

        max_event_counter = 0

        anim = ['|','/','-','\\','|','/','-','\\']

        ## Threshold scan
        for thr in range(fromThr, toThr, step):
            self.tpx.setThreshold(thr)
            self.tpx.openShutter()
            data = self.tpx.get_frame()
            thr_found = 0

            for d in data:
                if d.type == 0xA or d.type == 0xB:

                    ## get baseline
                    if thr == fromThr:
                        pre_thr[d.col][d.row] = d.event_counter

                    ## if ncnts increase compared to the last thr_dac, update baseline and thr_dac until no rise anymore
                    else:
                        if d.event_counter - pre_thr[d.col][d.row] > diff[d.col][d.row]:
                            diff[d.col][d.row] = d.event_counter - pre_thr[d.col][d.row]
                            res[d.col][d.row] = thr
                            thr_found += 1
                        # if d.col==128 and d.row==128:
                        #   print "pre=%d diff=%d now=%d thr=%d"%(pre_thr[d.col][d.row],diff[d.col][d.row],d.event_counter,res[d.col][d.row])

                        pre_thr[d.col][d.row] = d.event_counter

                    if d.event_counter > max_event_counter:
                        max_event_counter = d.event_counter

                elif d.type != 0x7:
                    logging.warning("Unexpected packet %s"%str(d))

            ## print some info
            if 1:
                print "%c %s %d/%d (packets %d) (found %d) (max counter %d)" % (13, anim[thr%len(anim)], thr, toThr, len(data), thr_found, max_event_counter),
                if thr == toThr-1: print
                sys.stdout.flush()

        ## return a matrix that contains the thr_dac at which the pixel count rate changes the most
        return res





    # Threshold Scan Gauss
    # ---------------------------------

    def threshold_scan_gauss(self, res=None, fromThr=1000, toThr=1300, step=5):
        peak = np.zeros((256,256), int)

        if res == None:
            res = {}
            for x in range(256):
                res[x] = {}
                for y in range(256):
                    res[x][y]=0

        cnt = {}
        for x in range(256):
            cnt[x] = {}
            for y in range(256):
                cnt[x][y] = {}
                cnt[x][y][0] = 0

        max_event_counter = 0

        anim = ['|','/','-','\\','|','/','-','\\']

        ## Threshold scan
        for thr in range(fromThr, toThr, step):
            self.tpx.setThreshold(thr)
            self.tpx.openShutter()
            data = self.tpx.get_frame()
            thr_found = 0

            for d in data:
                if d.type == 0xA or d.type == 0xB:
                    cnt[d.col][d.row][thr] = d.event_counter
                    if d.event_counter > max_event_counter:
                        max_event_counter = d.event_counter

                elif d.type != 0x7:
                    logging.warning("Unexpected packet %s"%str(d))

            ## print some info
            if 1:
                print "%c %s %d/%d (packets %d) (found %d) (max counter %d)" % (13, anim[thr%len(anim)], thr, toThr, len(data), thr_found, max_event_counter),
                if thr == toThr-1: print
                sys.stdout.flush()

        ## Find peak
        for x in range(256):
            for y in range(256):
                xdat = 0
                ydat = sorted(cnt[x][y])
                res[x][y] = curve_fit(xdat, ydat)

        ## return a matrix that contains the thr_dac at which the pixel count rate changes the most
        return res





    # Equalisation
    # ---------------------------------

    def eq_brute_force(self, path):
        scans = []
        avr = []

        ## Load threshold scan results
        for cdac in range(0, 16, 15):
            data = np.loadtxt(path + "/dacs%X_bl_thr.dat" % cdac)
            scans.append(data)

        avr0 = []
        avr1 = []
        X, Y = scans[0].shape
        bestval = np.zeros(scans[0].shape)
        bestcode = np.zeros(scans[0].shape)
        maskPixels = np.zeros(scans[0].shape)
        #print "%a %a" % (bestval, bestcode)

        ## Get average and mean, target value
        for x in range(X):
            for y in range(Y):
                if scans[0][x][y] > 0:
                    avr0.append(scans[0][x][y])
                if scans[1][x][y] > 0:
                    avr1.append(scans[1][x][y])
        avrmean0 = np.average(avr0)
        avrmean1 = np.average(avr1)
        target = (avrmean0 + avrmean1)/2
        print "DAC0 AVR=%.2f DAC15 AVR=%.2f -> TARGET=%.2f" % (avrmean0, avrmean1, target)
        #X, Y = scans[0].shape

        ## Get equalisation cdac
        for x in range(X):
            for y in range(Y):
                step = (abs(scans[0][x][y] - scans[1][x][y]))/15
                #print "(%d, %d) -> %.2f" % (x, y, step)
                bc = 0
                bv = scans[0][x][y]

                ## Find best cdac and calculate best value
                for i in range(16):
                    if abs(bv - target) > abs((step*i + scans[0][x][y]) - target):
                        bc = i
                        bv = step*i + scans[0][x][y]

                bestval[x][y] = bv
                bestcode[x][y] = bc

                ## Mask if the equalisation failed
                if abs(bestval[x][y] - target) > step:
                    maskPixels[x][y] = 1
                    bestval[x][y] = 0
                    bestcode[x][y] = 0

                else:
                    maskPixels[x][y] = 0

        np.savetxt(path + "/eq_bl.dat", bestval, fmt="%.2f")
        np.savetxt(path + "/eq_codes.dat", bestcode, fmt="%.0f")
        np.savetxt(path + "/eq_mask.dat", maskPixels, fmt="%d")
        print bestcode[122][134], bestcode[19][174], bestcode[89][200]



    # Execute
    # ---------------------------------

    def _execute(self, **keywords):
        self.tpx.resetPixels()
        self.tpx.shutterOff()
        self.tpx.setDacsDflt()



        # Set up
        # ---------------------------------

        # User Configuration
        polarity = 1            # polarity flag, holes = 0, electrons = 1
        testpulses = 0          # testpulse flag, equalise at baseline = 0, with testpulse = 1
        update = 1              # update dacs, trim and mask files in the config folder automatically

        ik_dac = 10             # discharge current dac
        pix_dac = 140           # trim bit dynamic range dac
        period_dac = 0x4        # sets duration between two testpulses, only relevant if testpulses = 1

        shutter_length = 0.001  # frame length in seconds

        npulses = 200           # number of testpulses to be sent, only relevant if testpulses = 1
        spacing = 2             # mask spacing, only relevant if testpulses = 1
        target = 50             # voltage to equalise at, only relevant if testpulses = 1



        # Preparation
        ## Set config directory
        bname = self.tpx.readName()
        fbase = ("config/%s" % bname)
        self.logging.info( "Chip name : %s" % bname)
        self.tpx.loadDACs(directory=fbase, chipname=bname)

        ## Set operating parameters
        self.tpx.setDecodersEna(True)
        self.tpx.setPllConfig((TPX3_PLL_RUN | TPX3_VCNTRL_PLL | TPX3_DUALEDGE_CLK | TPX3_PHASESHIFT_DIV_8 | TPX3_PHASESHIFT_NR_16 | 0x14<<TPX3_PLLOUT_CONFIG_SHIFT))
        if polarity:
            self.tpx.setGenConfig( TPX3_ACQMODE_EVT_ITOT | TPX3_POLARITY_EMIN )
        else:
            self.tpx.setGenConfig( TPX3_ACQMODE_EVT_ITOT )

        ## Set operating conditions
        self.logging.info("Optimization of DC operating point")
        self.tpx.setDac(TPX3_IBIAS_IKRUM, ik_dac)
        # self.tpx.setDac(TPX3_IBIAS_DISCS1_ON, 100)
        # self.tpx.setDac(TPX3_IBIAS_DISCS2_ON, 100)
        # self.tpx.setDac(TPX3_IBIAS_PREAMP_ON, 128)
        self.tpx.setDac(TPX3_IBIAS_PIXELDAC, pix_dac)
        self.tpx.setDac(TPX3_VFBK, 150)

        ## Print info
        self.logging.info("Trim dac current at %d, discharge current at %d." % (self.tpx.getDac(TPX3_IBIAS_PIXELDAC), self.tpx.getDac(TPX3_IBIAS_IKRUM)))
        self.logging.info("Chip Temperature is %.1f degrees" % self.tpx.getTpix3Temp())
        if testpulses:
            self.logging.info("Shutter length %d us" % int(((2 * (64*period_dac+1) * npulses)/40) + 100))
            self.logging.info("Pulse period %d us" % int(2*(64*period_dac+1)))
            self.logging.info("Number of testpulses: %d, Spacing: %dx%d" % (npulses, spacing, spacing))
        else:
            self.logging.info("Shutter length %d us" % shutter_length)


        ## Set ctpr bits
        self.tpx.setCtprBits(0)
        self.tpx.setCtpr()

        self.mkdir(self.fname)
        self.tpx.saveDACs(directory=self.fname, chipname=bname)
        if update:
            self.tpx.saveDACs(directory=fbase, chipname=bname)
            self.tpx.loadDACs(directory=fbase, chipname=bname)

        ## Set pixel config and readout
        self.tpx.resetPixelConfig()
        self.tpx.setPixelMask(ALL_PIXELS, ALL_PIXELS, 1)
        self.tpx.setPixelThreshold(ALL_PIXELS, ALL_PIXELS, 8)
        self.tpx.pauseReadout()
        self.tpx.setPixelConfig()
        # self.tpx.setDecodersEna()
        # self.tpx.sequentialReadout(tokens=4)
        self.tpx.sequentialReadout(tokens=1)

        ## Set shutter length
        self.tpx.setShutterLen(int(shutter_length * 1000000))

        ## Get time
        timestr = time.strftime("%Y%m%d_%H%M%S", time.localtime())



        # Run
        # ---------------------------------

        # Initial threshold Scan
        ## Initialise
        avr = []
        std = []

        mean_measured_not_eq = {}
        stdev_measured_not_eq = {}

        ## Threshold scan for highest and lowest cdac
        for cdac in range(0, 16, 15):
            logdir = self.fname + "/0x%0X/" % cdac
            logging.info("DACs %0x (logdir:%s)" % (cdac, logdir))
            self.mkdir(logdir)

            ## Reset data vector
            res = {}
            for x in range(256):
                res[x] = {}
                for y in range(256):
                    res[x][y] = 0

            ## Prepare testpulse settings if needed
            if testpulses:
                #genConfig_register |= TPX3_TESTPULSE_ENA
                self.tpx.setTpPeriodPhase(period_dac, 0)
                self.tpx.setTpNumber(npulses)
                shutter_length = int(((2 * (64*period_dac+1) * npulses)/40) + 100)
                self.tpx.setShutterLen(shutter_length)
                self.tpx.setDac(TPX3_VTP_COARSE, 100)
                self.tpx.setSenseDac(TPX3_VTP_COARSE)
                coarse = self.tpx.get_adc(32)
                self.tpx.setSenseDac(TPX3_VTP_FINE)
                time.sleep(0.001)
                best_vtpfine_diff = 1000
                best_vtpfine_code = 0

                ## Find best vtp_fine code
                for code in range(64, 512):
                    self.tpx.setDac(TPX3_VTP_FINE, code) # (0e-) slope 44.5e/LSB -> (112=1000e-)  (135=2000e-)
                    time.sleep(0.001)
                    fine = self.tpx.get_adc(16)
                    volts = 1000.0 * (fine-coarse)
                    if abs(volts-target) < abs(best_vtpfine_diff) and volts > 0:
                        best_vtpfine_diff = volts - target
                        best_vtpfine_code = code

                self.tpx.setDac(TPX3_VTP_FINE, best_vtpfine_code) # (0e-) slope 44.5e/LSB -> (112=1000e-)  (135=2000e-)
                time.sleep(0.001)
                fine = self.tpx.get_adc(32)
                self.logging.info( "  TPX3_VTP_FINE code=%d %.2f v_fine=%.1f v_coarse=%.1f" % (best_vtpfine_code, best_vtpfine_diff, fine*1000.0, coarse*1000.0))
                if polarity:
                    self.tpx.setGenConfig( TPX3_ACQMODE_EVT_ITOT | TPX3_POLARITY_EMIN | TPX3_TESTPULSE_ENA )
                else:
                    self.tpx.setGenConfig( TPX3_ACQMODE_EVT_ITOT | TPX3_TESTPULSE_ENA )


            ## Actual threshold scan
            for seq in range(spacing * spacing):
                logging.info("  seq %0d/%d"%(seq,spacing*spacing))
                self.tpx.resetPixelConfig()
                self.tpx.setPixelMask(ALL_PIXELS,ALL_PIXELS, 1)
                self.tpx.setPixelThreshold(ALL_PIXELS, ALL_PIXELS, cdac)
                self.tpx.setCtprBits(0)

                for x in range(256):
                    for y in range(256):
                        self.tpx.setPixelTestEna(x, y, testbit=False)
                        if x%spacing == int(seq/spacing) and y%spacing == seq%spacing:
                            self.tpx.setPixelMask(x, y, 0)
                            if testpulses:
                                self.tpx.setPixelTestEna(x, y, testbit=True)
                                self.tpx.setCtprBit(x, 1)

                self.tpx.pauseReadout()
                self.tpx.setPixelConfig()
                self.tpx.sequentialReadout(tokens=1)

                ## Threshold scan with testpulses
                if testpulses:
                    self.tpx.setCtpr()
                    res = self.threshold_scan_edge(res, fromThr=500, toThr=1500, threshold=npulses/2, step=2)

                ## Threshold scan with noise
                else:
                    if polarity:
                        res = self.threshold_scan_edge(res, fromThr=1400, toThr=400, threshold=10, step=-2)
                    else:
                        res = self.threshold_scan_edge(res, fromThr=800, toThr=2000, threshold=10, step=2)


            ## Calculate mean and std of threshold distribution at this cdac
            if 1:
                thr_level_array = []

                for col in range(256):
                    for row in range(256):
                        if res[col][row] > 0:
                            thr_level_array.append(res[col][row])
                mean_measured_not_eq[cdac] = np.mean(thr_level_array)
                stdev_measured_not_eq[cdac] = np.std(thr_level_array)

                print "DAC=0x%X MEAN=%.2f STDEV=%.2f %d" % (cdac, mean_measured_not_eq[cdac], stdev_measured_not_eq[cdac], len(thr_level_array))

                ## Save threshold map at this cdac
                fn = (self.fname + "/dacs%X_bl_thr.dat" % cdac)
                f = open(fn, "w")
                for col in range(256):
                    for row in range(256):
                        #f.write("%d " % (res[col][row])) #nalipour commented
                        f.write("%d " % (res[row][col])) #nalipour added
                    f.write("\n")
                f.close()



        # Rough Equalisation
        ## Find target threshold and dac
        target = (mean_measured_not_eq[15] - mean_measured_not_eq[0])/2 + mean_measured_not_eq[0]
        equalization_LSB = (mean_measured_not_eq[15] - mean_measured_not_eq[0])/15

        print "Target=%.2f eq_LSB=%.2f %d" % (target, equalization_LSB, int(target))

        ## Actual equalisation
        self.eq_brute_force(path=self.fname)

        ## Load results of equalisation
        eq_codes = np.loadtxt(self.fname + "/eq_codes.dat", dtype=int)
        eq_mask = np.loadtxt(self.fname + "/eq_mask.dat", dtype=int)
        print eq_codes[122][134], eq_codes[19][174], eq_codes[89][200]

        ## Reset data vector
        res = {}
        for x in range(256):
            res[x] = {}
            for y in range(256):
                res[x][y] = 0

        ## Scan over matrix
        for seq in range(spacing * spacing):
            logging.info("  seq %0d/%d" % (seq, spacing * spacing))
            self.tpx.resetPixelConfig()
            self.tpx.setPixelMask(ALL_PIXELS, ALL_PIXELS, 1)
            # self.tpx.setPixelThreshold(ALL_PIXELS, ALL_PIXELS, 0xf)
            self.tpx.setCtprBits(0)

            for x in range(256):
                for y in range(256):
                    self.tpx.setPixelTestEna(x, y, testbit=False)
                    self.tpx.setPixelThreshold(x, y, eq_codes[y][x])
                    if x%spacing == int(seq/spacing) and y%spacing == seq%spacing:
                        self.tpx.setPixelMask(x, y, eq_mask[y][x])
                        if testpulses:
                           self.tpx.setPixelTestEna(x, y, testbit=True)
                           self.tpx.setCtprBit(x, 1)

            self.tpx.pauseReadout()
            self.tpx.setPixelConfig()
            self.tpx.sequentialReadout(tokens=4)

            ## Scan with testpulses
            if testpulses:
                self.tpx.setCtpr()
                if polarity:
                    res = self.threshold_scan_edge(res, fromThr=int(target)+100, toThr=int(target)-100, threshold=npulses/2, step=-2)
                else:
                    res = self.threshold_scan_edge(res, fromThr=int(target)-100, toThr=int(target)+100, threshold=npulses/2, step=2)


            ## Scan with noise
            else:
                if polarity:
                    res = self.threshold_scan_edge(res, fromThr=int(target)+100, toThr=int(target)-100, threshold=5, step=-2)
                else:
                    res = self.threshold_scan_edge(res, fromThr=int(target)-100, toThr=int(target)+100, threshold=5, step=2)


        ## Save map and get mean
        fn = (self.fname + "/eq_bl_measured_%s.dat" % bname)
        f = open(fn, "w")
        thr_level_array = []

        for col in range(256):
            for row in range(256):
                #if res[row][col] > 0 and eq_mask[row][col] == 0: #nalipour commented
                if res[row][col] > 0 and eq_mask[col][row] == 0: #nalipour added
                    thr_level_array.append(res[row][col])
                else:
                    #eq_mask[row][col] = 1 #nalipour commented
                    eq_mask[col][row] = 1 #nalipour added
                f.write("%d " % (res[row][col]))
            f.write("\n")
        f.close()

        target_thr_tp = np.mean(thr_level_array)
        stdev_measured = np.std(thr_level_array)
        print "EQUALIZED!!! MEAN=%.2f STDEV=%.2f MASKED=%d" % (target_thr_tp, stdev_measured, 65536-len(thr_level_array))



        # Fine Equalisation
        ## Find new ideal cdac
        for fine_step in range(3):
            change_eq_neg = 0
            change_eq_pos = 0
            pixel_mask = 0
            pixel_mask_n = 0
            pixels_checked = 0

            for x in range(256):
                for y in range(256):
                    # if x%4 == int(seq/4) and y%4 == seq%4:
                    if not eq_mask[x][y]:
                        pixels_checked += 1
                        if res[y][x] < target_thr_tp - (equalization_LSB/2):
                            if eq_codes[x][y] < 15:
                                eq_codes[x][y] += 1
                                change_eq_pos += 1
                                #if x<20 and y<20:
                                #  print "NEG: %d %d %d %.2f %.2f %d"%(y,x,res[y][x],target_thr_tp, target_thr_tp - (equalization_LSB/2),eq_codes[y][x])
                            else:
                                eq_mask[x][y] = 1
                                pixel_mask += 1

                        if res[y][x] > target_thr_tp + (equalization_LSB/2):
                            if eq_codes[x][y] > 0:
                                eq_codes[x][y] -= 1
                                change_eq_neg += 1
                                # if x < 20 and y < 20:
                                #   print "POS: %d %d %d %.2f %.2f %d"%(y,x,res[y][x],target_thr_tp, target_thr_tp + (equalization_LSB/2),eq_codes[y][x])
                            else:
                                eq_mask[x][y] = 1
                                pixel_mask_n += 1

            print "FineStep%d -> Checked %d re-masked %d %d eq change+ %d eq change- %d\n" % (fine_step, pixels_checked, pixel_mask, pixel_mask_n, change_eq_pos, change_eq_neg)
            np.savetxt(self.fname + "/eq_codes_finestep_%s.dat" % (bname), eq_codes, fmt="%.0f")
            np.savetxt(self.fname + "/eq_mask_finestep_%s.dat" % (bname), eq_mask, fmt="%d")

            ## Reset data vector
            res = {}
            for x in range(256):
                res[x] = {}
                for y in range(256):
                    res[x][y] = 0

            ## Measure baseline
            for seq in range(spacing * spacing):
                logging.info("  seq %0d/%d" % (seq, spacing*spacing))
                self.tpx.resetPixelConfig()
                self.tpx.setPixelMask(ALL_PIXELS, ALL_PIXELS, 1)
                # self.tpx.setPixelThreshold(ALL_PIXELS, ALL_PIXELS, 0x0)
                self.tpx.setCtprBits(0)

                for x in range(256):
                    for y in range(256):
                        self.tpx.setPixelTestEna(x, y, testbit=False)
                        self.tpx.setPixelThreshold(x, y, eq_codes[y][x])
                        if x%spacing == int(seq/spacing) and y%spacing == seq%spacing:
                            self.tpx.setPixelMask(x, y, eq_mask[y][x])
                            if (testpulses):
                                self.tpx.setPixelTestEna(x, y, testbit=True)
                                self.tpx.setCtprBit(x, 1)

                self.tpx.pauseReadout()
                self.tpx.setPixelConfig()
                self.tpx.sequentialReadout(tokens=4)
                self.tpx.flush_udp_fifo(0x71FF000000000000) # flush until load matrix

                ## Scan with testpulses
                if testpulses:
                    self.tpx.setCtpr()
                    res = self.threshold_scan_edge(res, fromThr=int(target_thr_tp)-100, toThr=int(target_thr_tp)+100, threshold=npulses/2, step=1)

                ## Scan with noise
                else:
                    if polarity:
                        res = self.threshold_scan_edge(res, fromThr=int(target)+100, toThr=int(target)-100, threshold=5, step=-1)
                    else:
                        res = self.threshold_scan_edge(res, fromThr=int(target)-100, toThr=int(target)+100, threshold=5, step=1)


            ## Save baseline map
            fn = (self.fname + "/eq_bl_measured_finestep_%s.dat" % bname)
            f = open(fn, "w")
            thr_level_array = []
            for col in range(256):
                for row in range(256):
                    # if res[row][col] > 0 and eq_mask[row][col] == 0: # nalipour commented
                    if res[row][col] > 0 and eq_mask[col][row] == 0: # nalipour added
                        thr_level_array.append(res[row][col])
                    else:
                        #eq_mask[row][col] = 1 # nalipour commented
                        eq_mask[col][row] = 1 # nalipour added
                    f.write("%d " % (res[row][col]))
                f.write("\n")
            f.close()

            ## Save finestep dacs
            mean_measured = np.mean(thr_level_array)
            stdev_measured = np.std(thr_level_array)

            print "FineStep%d!!! MEAN=%.2f STDEV=%.2f MASKED=%d" % (fine_step, mean_measured, stdev_measured, 65536-len(thr_level_array))
            np.savetxt(self.fname + "/eq_codes_finestep_%s_f.dat" % (bname), eq_codes, fmt = "%.0f")
            np.savetxt(self.fname + "/eq_mask_finestep_%s_f.dat" % (bname), eq_mask, fmt = "% d")

        ## Save final results
        bfullname = "%s%04i_%s%02i" % ("W", int(bname.split("_")[0].split("W")[1]), bname.split("_")[1][0], int(bname.split("_")[1][1:]))
        self.LHCb_configStyle(self.fname + "/" + bfullname, eq_codes, eq_mask, 200, 400)

        if update:
            np.savetxt(fbase + "/eq_codes_finestep_%s_f.dat" % (bname), eq_codes, fmt = "%.0f")
            np.savetxt(fbase + "/eq_mask_finestep_%s_f.dat" % (bname), eq_mask, fmt = "%d")
            self.LHCb_configStyle(fbase + "/" + bfullname, eq_codes, eq_mask, 200, 400)
            self.saveConfiguration(fbase + "/config_%s_%s.t3x" % (bname, timestr), eq_codes, eq_mask, 1192)
