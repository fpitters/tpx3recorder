# ============================================================================
# File: test08_acq_datadriven.py
# ------------------------------
#
# Notes:
#   - Adapted from 'test28_northarea.py'.
#
# Assembly Notes:
#   1 W2_F2 target_thr_v=0.04 and ikrum=5 (Thr ~1150e- and TOT linear range up to ~130Ke-)
#   2 W2_F2 target_thr_v=0.06 and ikrum=5 (Thr ~1700e- and TOT linear range up to ~130Ke-)
#   3 W2_F2 target_thr_v=0.06 and ikrum=20 (Thr ~1950e- and TOT linear range up to ~490Ke-)
#   4 W2_F2 target_thr_v=0.06 and ikrum=60 (Thr ~2250e- and TOT linear range up to ~1300Ke-)
#   5 W2_C8 target_thr_v=0.04 and ikrum=20 (Thr ~1190e- and TOT linear range up to ~490Ke-) (Canberra Sensor)
#   6 W5_E11 target_thr_v=0.025 and ikrum=5 (Thr ~850 e-)
#   7 W5_E11 target_thr_v=0.04 and ikrum=5 (Thr ~850 e-)
#
# ============================================================================

import os
import random
import time
import logging
import shutil
import sys
import datetime
import numpy as np
import matplotlib.pyplot as plt
from tpx3_test import *
from SpidrTpx3_engine import *
from SpidrTpx3 import tpx3packet_hp
from dac_defaults import dac_defaults



# Definitions
# ============================================================================

def tot_to_energy(tot, *p):
    a, b, c, t = p
    if (a == 0) and (b != tot):
        e = (t * b + c - t * tot) / (b - tot)
    else:
        e = (t * a + tot - b + pow(pow(b+t*a-tot, 2) + 4*a*c, 0.5)) / (2 * a)
    return e

def energy_to_toa(energy, *p):
    c, t, d = p
    return (c/(energy - t)) + d




# Main Class
# ============================================================================

class test08_acq_datadriven(tpx3_test):
    """Take TOT HD and TOA HD in data driven acquisition"""

    def _execute(self, **keywords):
        self.tpx.resetPixels()
        self.tpx.setDacsDflt()
        self.tpx.shutterOff()

        # Configuration
        ## Main Parameters
        thr_dac = 1190
        ik_dac = 10
        polarity = 1            # polarity flag, holes = 0, electrons = 1
        shutter_length = 1 # in seconds
        nframes = 1 # max event number

        ## Flags
        w2f = 1
        w2g = 1

        ## Clock
        clk_period = 40

        if clk_period == 20:
            phase_shift = TPX3_PHASESHIFT_DIV_16
            BIN_HIST_HD = 16
            phases = 16.0
        elif clk_period == 40:
            phase_shift = TPX3_PHASESHIFT_DIV_8
            BIN_HIST_HD = 16
            phases = 16.0
        elif clk_period == 80:
            phase_shift = TPX3_PHASESHIFT_DIV_4
            BIN_HIST_HD = 8
            phases = 8.0
        elif clk_period == 160:
            phase_shift = TPX3_PHASESHIFT_DIV_2
            BIN_HIST_HD = 4
            phases = 4.0




        # Preparation
        ## Set config directory
        bname = self.tpx.readName()
        fbase = "config/%s/" % (bname)
        self.logging.info( "Chip name : %s" % bname)
        self.tpx.loadDACs(directory = fbase, chipname = bname)

        ## Set DACS
        self.tpx.setPllConfig((TPX3_PLL_RUN | TPX3_VCNTRL_PLL | TPX3_DUALEDGE_CLK | phase_shift | TPX3_PHASESHIFT_NR_16 | 0x14<<TPX3_PLLOUT_CONFIG_SHIFT))
        self.tpx.setOutputMask(0xFF)
        if polarity:
            self.tpx.setGenConfig(TPX3_ACQMODE_TOA_TOT  | TPX3_FASTLO_ENA | TPX3_GRAYCOUNT_ENA | TPX3_POLARITY_EMIN)
        else:
            self.tpx.setGenConfig(TPX3_ACQMODE_TOA_TOT  | TPX3_FASTLO_ENA | TPX3_GRAYCOUNT_ENA)

        ## Set operating conditions
        self.logging.info("Optimization of DC operating point")
        self.tpx.setDac(TPX3_IBIAS_IKRUM, ik_dac)
        # self.tpx.setDac(TPX3_IBIAS_DISCS1_ON, 100)
        # self.tpx.setDac(TPX3_IBIAS_DISCS2_ON, 128)
        # self.tpx.setDac(TPX3_IBIAS_PREAMP_ON, 128)
        # self.tpx.setDac(TPX3_IBIAS_PIXELDAC, 128)
        # self.tpx.setDac(TPX3_VFBK, 164)

        ## Reset pixels
        self.tpx.resetPixelConfig()


        ## Load equalisation codes and mask in Xavi's format
        if 0:
            codes_eq = np.loadtxt(fbase + "/eq_codes_finestep_%s.dat" % bname, int)
            mask = np.loadtxt(fbase + "/eq_mask_finestep_%s.dat" % bname, int)
            self.logging.info( "Mask and trim dacs loaded from Xavi's format.")

        ## Load equalisation codes and mask in LHCb format
        if 1:
            bfullname = "%s%04i_%s%02i" % ("W", int(bname.split("_")[0].split("W")[1]), bname.split("_")[1][0], int(bname.split("_")[1][1:]))
            trim = np.loadtxt(fbase + "/%s_trimdacs.txt" % (bfullname))
            codes_eq = np.zeros((256, 256), int)
            mask = np.zeros((256, 256), int)
            for x in range(256):
                for y in range(256):
                    codes_eq[y][x] = trim[x*256+y][2]
                    mask[y][x] = trim[x*256+y][3]
            self.logging.info( "Mask and trim dacs loaded from LHCb format.")

        self.logging.info( "Threshold at %d, discharge current at %d." % (thr_dac, self.tpx.getDac(TPX3_IBIAS_IKRUM)))
        self.logging.info( "Datadriven acquisition. Recording for %d seconds or %d frames." % (shutter_length, nframes))
        self.logging.info( "Chip Temperature is %.1f degrees" % self.tpx.getTpix3Temp())

        for x in range(256):
            for y in range(256):
                self.tpx.setPixelThreshold(x, y, codes_eq[y][x])
                self.tpx.setPixelMask(x, y, mask[y][x])

        ## Mask noisy pixels by hand
        self.tpx.setPixelMask(8, 5, 1) # W5_E2
        self.tpx.setPixelMask(8, 6, 1) # W5_E2
        self.tpx.setPixelConfig()

        ## Set ctpr bits
        self.tpx.setCtprBits(0)
        self.tpx.setCtpr()

        self.mkdir(self.fname)
        self.tpx.setThreshold(thr_dac)
        self.tpx.saveDACs(directory=self.fname, chipname=bname)

        ## Set output files
        cnt_file = 0
        fout = self.fname + '/data_%d.dat' % cnt_file
        f = open(fout, "w")
        self.logging.info("Storing raw hits to %s" % fout)
        f.write("# Settings: Shutter Length = %d s, Threshold = %d dacs" % (shutter_length, thr_dac))
        f.write("# pix_col\tpix_row\ttot\ttot_hd\ttoa(ns)\toa_hd(ns)\ttemp\n")

        ## Set pixel config and readout
        self.mask_bad_pixels()
        self.tpx.pauseReadout()
        self.tpx.setPixelConfig()
        self.tpx.datadrivenReadout()
        self.tpx.resetPixels()

        ## Set shutter control
        if shutter_length >= 1000:
            self.tpx.shutterOn() # open Shutter by PC
            print "shutter on"
        else:
            self.tpx.setShutterLen(int(shutter_length * 1000000)) # shutter controlled by FPGA
            self.tpx.openShutter(sleep = False)
            print "opened shutter"

        ## Sync timing
        self.tpx.resetTimer()
        self.tpx.t0Sync()

        time_start = time.time()
        time_last_temp = time.time()
        time_elapsed = 0.0



        # Run
        ## Initialise Variables
        cnt_evnt = 0
        cnt_total = 0
        cnt_frame = 0
        pc_cnt = np.zeros((256,256), int)
        itot_cnt = np.zeros((256,256), int)

        line = ""
        finish = False

        ## Take data
        while not finish:
            temp = 1#self.tpx.getTpix3Temp()
            time_elapsed = time.time() - time_start

            ## Through away first millisecond
            if time_elapsed < 0.001:
                data = self.tpx.get_N_packets(1024)

            ## Stop if acquisition time is over and pc controlled shutter
            if (time_elapsed > acquisition_time) and (acquisition_time > 1000):
                self.tpx.shutterOff()
                time.sleep(0.001)
                data = self.tpx.get_frame()
                finish = True

            ## Stop if acqisition time is over
            if time_elapsed > acquisition_time:
                self.tpx.shutterOff()
                finish = True

            ## Stop if event counter is high enough
            if cnt_frame > nframes:
                self.logging.info("Desired number of events reached")
                self.tpx.shutterOff()
                finish = True

            ## Else continue to take data
            # else: # new fw
            #     data = self.tpx.daq.getSample(1024, 100) # (max size, timeout_ms)
            #     if data:
            #         raw_pck = self.tpx.daq.nextPacket()
            #
            #         ## Process the data to something readable
            #         while raw_pck:
            #             pck = tpx3packet_hp(raw_pck)

            ## Else continue to take data
            else: # old fw
                data = self.tpx.get_N_packets(1024)
                raw_pck = True
                if raw_pck:
                    for pck in data:

                        ## Check if data is good
                        if pck.isData():
                            print 'hello'
                            if pck.tot > 0:
                                ftoa_corrected = pck.ftoa + self.tpx.clockPhase(col=pck.col, phase_num=phases)
                                tot_hd = pck.tot + pck.ftoa/phases - 0.5
                                toa_tc = pck.toa * 25 - ftoa_corrected * 25./16
                                line += "%d\t%d\t%d\t%.4f\t%.4f\t%.1f\n" % (pck.col, pck.row, pck.tot, tot_hd, toa_tc, temp)
                                cnt_evnt += 1
                                pc_cnt[pck.col][pck.row] += 1
                                itot_cnt[pck.col][pck.row] += tot_hd

                            cnt_total += cnt_evnt

                            ## Simulate frame if counter is above 150
                            if cnt_evnt == 180:
                                line += "#\n"
                                cnt_frame += 1
                                cnt_evnt = 0
                                #temp = self.tpx.getTpix3Temp()

                            ## Print start info
                            if cnt_frame < 100:
                                print "Acquisition running ok. %d events in this frame. %d in total in the first %d frames." % (cnt_event, cnt_total, frame)

                            ## Print log info
                            if cnt_frame%1000 == 0:
                                self.logging.info("Frame Nr. %d, Temperature %.1fC, Number of readout pixels %d" % (frame, temp, cnt_event))
                                f.write(line)
                                f.flush()

                            ## Change file
                            if cnt_frame%100000 == 0:
                                f.close()
                                cnt_file += 1
                                fout = self.fname + '/data_%d.dat' % cnt_file
                                f = open(fout, "w")
                                self.logging.info("Storing raw hits to %s" % fout)
                                f.write("# Settings: Shutter Length = %d s, Threshold = %d dacs" % (shutter_length, thr_dac))
                                f.write("# pix_col\tpix_row\ttot\ttot_hd\ttoa(ns)\toa_hd(ns)\ttemp\n")

                        ## Else return error and stop
                        else:
                            if pck.isEoR():
                                print "No data"
                                f.write(line)
                                f.flush()
                                finish = True

                        # raw_pck = self.tpx.daq.nextPacket()

        self.warning_detailed_summary()
        self.logging.info("Events collected %d" % cnt_total)
        f.close()



        # Finish
        ## Write to file
        if w2f:
            self.save_np_array(pc_cnt, fn = self.fname + '/pc.map')
            self.save_np_array(itot_cnt, fn = self.fname + '/itot.map')

        ## Print graph
        if w2g:
            col = []
            row = []
            val = []
            for i in range(256):
                for j in range(256):
                    col.append(i)
                    row.append(j)
                    val.append(pc_cnt[i][j])

            fig, ax = plt.subplots()
            ax.set_title(pn, fontsize=24)
            ax.set_xlabel('column', fontsize=22)
            ax.set_ylabel('row', fontsize=22)
            plt.hist2d(col, row, bins=256, range=[[0, 255], [0, 255]], weights=val)
            plt.colorbar()
            plt.savefig(self.fname + "/pc_map.png", bbox_inches="tight")
            plt.clf()
            self.logging.info("Saving graph to file %s" % (self.fname + "/pc_map.png"))
