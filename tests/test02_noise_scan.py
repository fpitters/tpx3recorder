# ============================================================================
# File: test02_noise_scan.py
# ------------------------------
#
# Notes:
#   - Adapted from 'test08_noise_scan.py'.
#   - Made configuration more obvious
#   - Running in EVT_ITOT mode
#
# Layout:
#   configure and prepare
#   for each mask:
#       for each threshold:
#           record how many events are seen for each pixel
#
#
# ============================================================================

from tpx3_test import *
from SpidrTpx3_engine import *
from scipy.optimize import curve_fit
from dac_defaults import dac_defaults
import os
import sys
import random
import time
import logging
import numpy as np
import matplotlib.pyplot as plt




# Definitions
# ============================================================================

def gauss(x, *p):
    A, mu, sigma = p
    return A * np.exp(-(x-mu)**2 / (2. * sigma**2))




# Main
# ============================================================================

class test02_noise_scan(tpx3_test):
    """Threshold scan over noise floor, uses EVT_ITOT mode

    Parameters:
    polarity ... polarity flag, holes = 0, electrons = 1
    shutter_length ... frame length in seconds
    """


    # Threshold Scan
    # ---------------------------------

    def threshold_scan(self, res=None, seq=0):
        self.warning_detailed_restart()
        peak = np.zeros((256, 256), int)
        mask = 0
        anim = ['|','/','-','\\','|','/','-','\\']

        for i in range(0, 512, 1):
            self.tpx.setDac(TPX3_VTHRESH_FINE, i)
            self.tpx.resetPixels()
            data = self.tpx.recv_mask(0x7102000000000000, 0xFFFF000000000000)

            self.tpx.openShutter()
            data = self.tpx.get_frame()

            # sanity check
            if len(data) == 0:
                 self.logging.error("No data !!")
            elif data[-1].raw&0xFFFF00000000 != 0x71A000000000:
                 self.logging.error("Last packet %s" % (str(data[-1])))
                 self.logging.error("Packets received %d (to be masked %d)" % (len(data), mask))

            # graphical progress bar
            if 1:
                print "%s %d/%d (packets %d)%c" % (anim[i%len(anim)], i, 512, len(data), 13),
                if i == 511: print
                sys.stdout.flush()

            # data analysis
            for d in data:
                self.logging.debug(str(d))
                if d.type == 0xA:
                    if d.itot < 0:
                        self.warning_detailed("Bad itot : " + str(d))
                    if d.event_counter < 0:
                        self.warning_detailed("Bad event counter : "+str(d))
                    if d.col%2 != int(seq/2) and d.row%2 != seq%2:
                        self.warning_detailed("Unexpected pixel (%d,%d) in seq %d" % (d.col, d.row, seq))

                    res[d.col][d.row][i] = d.event_counter

                    if  d.event_counter >= 200 and peak[d.col][d.row] == 0:
                        peak[d.col][d.row] = 1
                    elif d.event_counter <= 2 and peak[d.col][d.row] == 1:
                        self.tpx.setPixelMask(d.col, d.row, 1)
                        mask += 1
                        peak[d.col][d.row] = 2
                elif d.type != 0x7:
                  self.logging.warning("Unexpected packet %s" % str(d))

            if mask > 7500:
                self.logging.debug("Masking %d pixels" % mask)
                self.tpx.pauseReadout()
                self.tpx.setPixelConfig()
                self.tpx.sequentialReadout()
                self.tpx.resetPixels()
                self.tpx.flush_udp_fifo(0x71FF000000000000) # flush until load matrix
                mask = 0

        self.warning_detailed_summary()

        return res



    # Threshold Scan Xray
    # ---------------------------------

    def threshold_scan_xray(self, res=None, fromThr=0, toThr=512, threshold=10, step=1, seq=0, to_test=65536):
        peak = np.zeros((256, 256), int)

        ## initialise data vector
        if res == None:
            res = {}
            for x in range(256):
                res[x] = {}
                for y in range(256):
                    res[x][y] = 0

        mask = 0
        pixels_detected = 0;
        anim = ['|','/','-','\\','|','/','-','\\']

        ## threshold scan
        for i in range(fromThr, toThr, step):
            self.tpx.setThreshold(i)

            if 0 and len(data) > 1:
                for d in data:
                    logging.warning("after setdac %s" % (str(d)))

            self.tpx.openShutter()
            data = self.tpx.get_frame()

            ## print output
            if 1:
                print "%c %s %d/%d (packets %d) %d/%d" % (13, anim[i%len(anim)], i, toThr, len(data), pixels_detected, to_test),
                if i == toThr - 1: print
                sys.stdout.flush()

            ## process data
            for d in data:
                logging.debug(str(d))

                if d.type == 0xA or d.type == 0xB:
    	            if d.itot < 0:
                        self.warning_detailed("Bad itot : " + str(d))
                    if d.event_counter < 0:
                        self.warning_detailed("Bad event counter : " + str(d))
                    #if d.col%2 != int(seq/2) and d.row%2 != seq%2:
                    #   self.warning_detailed("Unexpected pixel (%d,%d) in seq %d" % (d.col, d.row, seq))

                    res[d.col][d.row][i] = d.event_counter

                    if  d.event_counter >= 10 and peak[d.col][d.row] == 0:
                        peak[d.col][d.row] = 1
                    elif d.event_counter <= 2 and peak[d.col][d.row] == 1:
                        self.tpx.setPixelMask(d.col, d.row, 1)
                        mask += 1
                        peak[d.col][d.row] = 2

                    pixels_detected += 1

                elif d.type != 0x7:
                    logging.warning("Unexpected packet %s" % str(d))

            # process mask
            if mask > 7500:
                logging.debug("Masking %d pixels" % mask)
                # self.tpx.flushFifoIn()
                self.tpx.pauseReadout()
                self.tpx.setPixelConfig()
                self.tpx.sequentialReadout(tokens=4)
                self.tpx.flush_udp_fifo(0x71FF000000000000) # flush until load matrix

                mask = 0

        ## exit loop
    	# if  pixels_detected == to_test:
    	#    break

        ## return a matrix that contains the number of events counted for each threshold
        return res



    # Execute
    # ---------------------------------

    def _execute(self, **keywords):
        self.tpx.resetPixels()
        self.tpx.shutterOff()
        self.tpx.setDacsDflt()



        # Set up
        # ---------------------------------

        # User configuartion
        polarity = 1            # polarity flag, holes = 0, electrons = 1
        shutter_length = 0.001  # frame length in seconds


        # Preparation
        ## Set config directory
        bname = self.tpx.readName()
        fbase = "config/%s/" % (bname)
        self.logging.info( "Chip name    : %s" % bname)
        self.tpx.loadDACs(directory=fbase, chipname=bname)

        ## Prepare device
        eth_filter, cpu_filter = self.tpx.getHeaderFilter()
        self.tpx.setHeaderFilter(0x0c80, cpu_filter)

        ## Set operating parameters
        # self.tpx.setGenConfig( TPX3_ACQMODE_EVT_ITOT | TPX3_SELECTTP_DIGITAL )
        self.tpx.setGenConfig( TPX3_ACQMODE_EVT_ITOT | TPX3_SELECTTP_DIGITAL | TPX3_GRAYCOUNT_ENA )
        self.tpx.setPllConfig( (TPX3_PLL_RUN | TPX3_VCNTRL_PLL | TPX3_DUALEDGE_CLK | TPX3_PHASESHIFT_DIV_8 | TPX3_PHASESHIFT_NR_16 | 0x14<<TPX3_PLLOUT_CONFIG_SHIFT) )

        ## Print some info
        self.logging.info("Shutter length is %d us" % int(shutter_length * 1000000))
        self.logging.info("Trim dac current at %d, discharge current at %d." % (self.tpx.getDac(TPX3_IBIAS_PIXELDAC), self.tpx.getDac(TPX3_IBIAS_IKRUM)))
        self.logging.info("Chip Temperature is %.1f degrees" % self.tpx.getTpix3Temp())

        ## Set ctpr bits
        self.tpx.setCtprBits(0)
        self.tpx.setCtpr()

        ## Set shutter time
        self.tpx.setShutterLen(int(shutter_length * 1000000))

        ## Set readout
        self.tpx.sequentialReadout()
        self.tpx.setLogLevel(2) # LVL_WARNING

        ## Reset pixels
        self.tpx.resetPixelConfig()

        ## Load equalisation codes and mask in Xavi's format
        if 0:
            codes_eq = np.loadtxt(fbase + "/eq_codes_finestep_%s_f.dat" % bname, int)
            mask = np.loadtxt(fbase + "/eq_mask_finestep_%s_f.dat" % bname, int)
            self.logging.info( "Mask and trim dacs loaded from Xavi's format.")

        ## Load equalisation codes and mask in LHCb format
        if 1:
            bfullname = "%s%04i_%s%02i" % ("W", int(bname.split("_")[0].split("W")[1]), bname.split("_")[1][0], int(bname.split("_")[1][1:]))
            trim = np.loadtxt(fbase + "/%s_trimdacs.txt" % (bfullname))
            codes_eq = np.zeros((256, 256), int)
            mask = np.zeros((256, 256), int)
            for x in range(256):
                for y in range(256):
                    codes_eq[y][x] = trim[x*256+y][2]
                    mask[y][x] = trim[x*256+y][3]
            self.logging.info("Mask and trim dacs loaded from LHCb format.")

        logdir = self.fname + "/details/"

        ## Print info
        self.logging.info("Chip Temperature is %.1f degrees" % self.tpx.getTpix3Temp())



        # Run
        # ---------------------------------

        ## Initialise data vector
        res = {}
        for x in range(256):
            res[x] = {}
            for y in range(256):
                res[x][y] = {}

        ## Performe noise scans in 4 steps (one pixel out of 4 active at the time)
        spacing_x = 2
        spacing_y = 2
        seq_total = spacing_x * spacing_y

        for seq in range(seq_total):
            self.logging.info("  seq %0d/%0d" % (seq, seq_total))
            cdac = 8
            self.tpx.resetPixelConfig()
            self.tpx.setPixelMask(ALL_PIXELS, ALL_PIXELS, 1)
            self.tpx.setPixelThreshold(ALL_PIXELS, ALL_PIXELS, cdac)
            on = 0

            for x in range(256):
            	for y in range(256):
            	    self.tpx.setPixelThreshold(y, x, codes_eq[x][y])
            	    #self.tpx.setPixelThreshold(x, y, cdac)
                    if x % spacing_x == (int)(seq/spacing_y) and y % (spacing_y) == seq%(spacing_y):
                        self.tpx.setPixelMask(y, x, mask[x][y])
                    if not mask[x][y]:
                        on += 1;

            self.mask_bad_pixels()

            self.tpx.pauseReadout()
            self.tpx.setPixelConfig()
            self.tpx.sequentialReadout()
            self.tpx.flush_udp_fifo(0x71FF000000000000) # flush until load matrix

            if polarity:
            	res = self.threshold_scan_xray(res, fromThr=800, toThr=1600, threshold=10, step=2, seq=seq, to_test=on)
            else:
            	res = self.threshold_scan_xray(res, fromThr=1300, toThr=400, threshold=10, step=-2, seq=seq, to_test=on)


        ## Process data
        fit = 1
        w2f = True

        if fit:
            bad_pixels = []
            missing_pixels = []
            fit_res = {}
            mean_values = []
            rms_values = []
            peak_values = []

            self.warning_detailed_restart()
            for col in range(256):
                fit_res[col] = {}
                if w2f:
                    self.mkdir(logdir + "/%03d" % col)

                if col in res:
                    print "Fitting col %d %c" % (col, 13),
                    sys.stdout.flush()

                else:
                    self.warning_detailed("No hits in col %d" % col)
                    for row in range(256):
                        missing_pixels.append( (col,row) )
                        fit_res[col][row] = [-1.0, -1.0, -1.0]
                    continue

                for row in range(256):
                    if col in res and row in res[col]:
                        if w2f:
                            fn = logdir + "/%03d/%03d_%03d.dat" % (col,col,row)
                            f = open(fn, "w")
                        codes = []
                        vals = []
                        avr = 0.0
                        N = 0

                        for code in sorted(res[col][row]):
                           val = res[col][row][code]
                           if w2f and val > 0:
                             f.write("%d %d\n" % (code, val))
                           if val > 2:
                             codes.append(code)
                             vals.append(val)
                             avr += code * val
                             N += val

                        fit_res[col][row] = [-1.0, -1.0, -1.0]
                        if N > 2:
                            avr = avr / N
                            try:
                                p0 = [max(vals), avr, 4.]
                                coeff, var_matrix = curve_fit(gauss, codes, vals, p0=p0)
                                peak = coeff[0]
                                mean = coeff[1]
                                rms = coeff[2]
                                if mean < 400 or mean > 1600:
                                    mean = 1000.0
                                    bl_off_pixels.add( (col,row) )
                                if rms < 2 or rms > 30:
                                    mean = 6.0
                                    noise_off_pixels.add( (col,row) )

                                mean_values.append(mean)
                                rms_values.append(rms)
                                peak_values.append(peak)
                                fit_res[col][row] = coeff
                            except:
                                pass

                        ## Deal with dead or masked pixels
                        else:
                            if not (col,row) in self.bad_pixels:
                                self.warning_detailed("No hits for pixel (%d,%d)" % (col,row))
                                missing_pixels.append( (col,row) )

                        if w2f:
                            f.write("# avr:%.3f rms:%.3f\n" % (fit_res[col][row][1], fit_res[col][row][2]))
                            f.close()

            self.warning_detailed_summary()


            ## Analysis and finish
            bl_mean = np.mean(mean_values)
            bl_rms = np.std(mean_values)
            noise_mean = np.mean(rms_values)
            noise_rms = np.std(rms_values)

            if polarity:
                thr_op = bl_mean + 6 * pow(pow(bl_rms, 2) + pow(noise_mean, 2), 0.5)
            else:
                thr_op = bl_mean - 6 * pow(pow(bl_rms, 2) + pow(noise_mean, 2), 0.5)

            self.results["BL_MEAN"] = bl_mean
            self.results["BL_RMS"] = bl_rms
            self.results["NOISE_MEAN"] = noise_mean
            self.results["NOISE_RMS"] = noise_rms

            ## Print mean values
            self.logging.info("")
            self.logging.info("Baseline %3.2f std.dev. %3.2f" % (bl_mean, bl_rms))
            self.logging.info("Noise %3.2f std.dev. %3.2f" % (noise_mean, noise_rms))
            self.logging.info("")
            self.logging.info("Recommended operational threshold: %d (6 sigmas away from baseline)", thr_op)
            self.logging.info("")

            fn = (self.fname + "/bl.map", self.fname + "/rms.map", self.fname + "/problematic.map", self.fname + "/peak.map")
            f = (open(fn[0], "w"), open(fn[1], "w"), open(fn[2], "w"), open(fn[3], "w"))

            ## Determine bad pixels
            bl_off_pixels = []
            noise_off_pixels = []
            for row in range(256):
                for col in range(256):
                    if col in fit_res and row in fit_res[col]:
                        bl = fit_res[col][row][1]
                        noise = fit_res[col][row][2]
                        peak = fit_res[col][row][0]

                        if fit_res[col][row][2] < -1.0:
                            noise =- fit_res[col][row][2]

                        problem = 0
                        if bl > 0 and (bl > bl_mean + 6.0 * bl_rms or bl < bl_mean - 6.0 * bl_rms):
                            bl_off_pixels.append((col,row))
                            problem = 1
                        if noise > 0 and (noise > noise_mean * 1.5 or noise < noise_mean * 0.5):
                            noise_off_pixels.append((col,row))
                            problem = 2
                        if bl < 0 or noise < 0:
                            problem = 3

                        f[0].write("%.3f " % bl)
                        f[1].write("%.3f " % noise)
                        f[2].write("%d " % problem)
                        f[3].write("%d " % peak)


                for i in range(4):
                  f[i].write("\n")
            for i in range(4):
              f[i].close()

            self.logging.info("")
            self.warn_info("Missing pixels (very distant baseline?) (%d) : %s" % (len(missing_pixels), str(missing_pixels)), len(missing_pixels) > 0)
            self.warn_info("Pixels with distant baseline (%d) : %s" % (len(bl_off_pixels), str(bl_off_pixels)), len(bl_off_pixels) > 0)
            self.warn_info("Pixels with distant noise (%d) : %s" % (len(noise_off_pixels), str(noise_off_pixels)), len(noise_off_pixels) > 0)

            self.add_bad_pixels(missing_pixels)
            self.add_bad_pixels(bl_off_pixels)
            self.add_bad_pixels(noise_off_pixels)

            self.results["MISSING"] = len(missing_pixels)
            self.results["DISTANT_BASELINE"] = len(bl_off_pixels)
            self.results["DISTANT_NOISE"] = len(noise_off_pixels)

            self.logging.info("")
            self.logging.info("Saving baseline map to %s" % fn[0])
            self.logging.info("Saving noise map to %s" % fn[1])
            self.logging.info("Saving bad pixels map to %s" % fn[2])

            self.logging.info("")

            if w2f:
                aname = logdir[:-1] + ".zip"
                self.logging.info("Creating archive %s" % aname)
                self.zipdir(aname, logdir)

            ## Save results in list style
            fout = open(self.fname + '/tp_calibration.dat', "w")
            fout.write("#col\trow\tbl\trms\tproblematic\tpeak\n")
            for col in range(0, 256):
                for row in range(0, 256):
                    fout.write("%d\t%d\t%10.5f\t%10.5f\t%d\t%d\t\n" % (col, row, bl[col][row], rms[col][row], problem[col][row], peak[col][row]))
            fout.close()

        return
