# ============================================================================
# File: test08_acq_framebased.py
# ------------------------------
#
# Notes:
#   - Adapted from 'test28_northarea.py'.
#
# Layout:
#   configure and prepare
#   for every frame:
#       open shutter and take data
#       write to file
#   save and finish
#
# Assembly Notes:
#   W0005_E02: nominal thr_dac=1190
#
# Status:
#   Works well
#
# ============================================================================

import os
import random
import time
import logging
import shutil
import sys
import datetime
import numpy as np
import matplotlib.pyplot as plt
from tpx3_test import *
from SpidrTpx3_engine import *
from dac_defaults import dac_defaults
from scipy.optimize import curve_fit



# Definitions
# ============================================================================

def tot_to_energy(tot, *p):
    a, b, c, t = p
    if (a == 0) and (b != tot):
        e = (t * b + c - t * tot) / (b - tot)
    else:
        e = (t * a + tot - b + pow(pow(b+t*a-tot, 2) + 4*a*c, 0.5)) / (2 * a)
    return e

def energy_to_toa(energy, *p):
    c, t, d = p
    if (energy - t) == 0:
        T = 1000
    else:
        T = (c / (energy - t)) + d
    return T




# Main Class
# ============================================================================

class test08_acq_framebased(tpx3_test):
    """Take TOT HD and TOA HD in framebased acquisition

    Parameters:
    polarity ... polarity flag, holes = 0, electrons = 1
    thr_dac ... threshold dac
    ik_dac ... discharge current dac
    shutter_length ... frame length in seconds
    nframes ... number of frames
    """

    def _execute(self, **keywords):
        self.tpx.resetPixels()
        self.tpx.shutterOff()
        self.tpx.setDacsDflt()


        # Configuration
        polarity = 1            # polarity flag, holes = 0, electrons = 1
        thr_dac = 1190          # threshold dac
        ik_dac = 10             # discharge current dac
        shutter_length  = 1     # frame length in seconds
        nframes = 10            # number of frames

        ## Flags
        w2f = 1 # write to file
        w2g = 0 # write to graph

        ## Graph
        nbins = 100

        ## Clock
        clk_period = 40

        if clk_period == 20:
            phase_shift = TPX3_PHASESHIFT_DIV_16
            BIN_HIST_HD = 16
            phases = 16.0
        elif clk_period == 40:
            phase_shift = TPX3_PHASESHIFT_DIV_8
            BIN_HIST_HD = 16
            phases = 16.0
        elif clk_period == 80:
            phase_shift = TPX3_PHASESHIFT_DIV_4
            BIN_HIST_HD = 8
            phases = 8.0
        elif clk_period == 160:
            phase_shift = TPX3_PHASESHIFT_DIV_2
            BIN_HIST_HD = 4
            phases = 4.0



        # Preparation
        ## Set config directory
        bname = self.tpx.readName()
        fbase = "config/%s/" % (bname)
        self.logging.info( "Chip name : %s" % bname)
        self.tpx.loadDACs(directory = fbase, chipname = bname)

        ## Set operating parameters
        self.tpx.setPllConfig((TPX3_PLL_RUN | TPX3_VCNTRL_PLL | TPX3_DUALEDGE_CLK | phase_shift | TPX3_PHASESHIFT_NR_16 | 0x14<<TPX3_PLLOUT_CONFIG_SHIFT))
        self.tpx.setOutputMask(0xFF)
        if polarity:
            self.tpx.setGenConfig(TPX3_ACQMODE_TOA_TOT  | TPX3_FASTLO_ENA | TPX3_GRAYCOUNT_ENA | TPX3_POLARITY_EMIN)
        else:
            self.tpx.setGenConfig(TPX3_ACQMODE_TOA_TOT  | TPX3_FASTLO_ENA | TPX3_GRAYCOUNT_ENA)

        ## Set operating conditions
        self.logging.info("Optimization of DC operating point")
        self.tpx.setDac(TPX3_IBIAS_IKRUM, ik_dac)
        # self.tpx.setDac(TPX3_IBIAS_DISCS1_ON, 100)
        # self.tpx.setDac(TPX3_IBIAS_DISCS2_ON, 128)
        # self.tpx.setDac(TPX3_IBIAS_PREAMP_ON, 128)
        # self.tpx.setDac(TPX3_IBIAS_PIXELDAC, 128)
        # self.tpx.setDac(TPX3_VFBK, 164)

        ## Reset pixels
        self.tpx.resetPixelConfig()

        ## Load equalisation codes and mask in Xavi's format
        if 0:
            codes_eq = np.loadtxt(fbase + "/eq_codes_finestep_%s.dat" % bname, int)
            mask = np.loadtxt(fbase + "/eq_mask_finestep_%s.dat" % bname, int)
            self.logging.info( "Mask and trim dacs loaded from Xavi's format.")

        ## Load equalisation codes and mask in LHCb format
        if 1:
            bfullname = "%s%04i_%s%02i" % ("W", int(bname.split("_")[0].split("W")[1]), bname.split("_")[1][0], int(bname.split("_")[1][1:]))
            trim = np.loadtxt(fbase + "/%s_trimdacs.txt" % (bfullname))
            codes_eq = np.zeros((256, 256), int)
            mask = np.zeros((256, 256), int)
            for x in range(256):
                for y in range(256):
                    codes_eq[y][x] = trim[x*256+y][2]
                    mask[y][x] = trim[x*256+y][3]
            self.logging.info( "Mask and trim dacs loaded from LHCb format.")

        self.logging.info( "Threshold at %d, discharge current at %d." % (thr_dac, ik_dac))
        self.logging.info( "Frame-based acquisition. Recording %d frames of %.3f seconds each." % (nframes, shutter_length))
        self.logging.info( "Chip Temperature is %.1f degrees" % self.tpx.getTpix3Temp())

        for x in range(256):
            for y in range(256):
                self.tpx.setPixelThreshold(x, y, codes_eq[y][x])
                self.tpx.setPixelMask(x, y, mask[y][x])

        ## Mask noisy pixels by hand
        # self.tpx.setPixelMask(8, 5, 1) # W5_E2
        # self.tpx.setPixelMask(8, 6, 1) # W5_E2
        # self.tpx.setPixelMask(7, 5, 1) # W5_E2
        # self.tpx.setPixelMask(9, 7, 1) # W5_E2
        # self.tpx.setPixelMask(78, 209, 1)
        self.tpx.setPixelConfig()

        ## Set ctpr bits
        self.tpx.setCtprBits(0)
        self.tpx.setCtpr()

        self.mkdir(self.fname)
        self.tpx.setThreshold(thr_dac)
        self.tpx.saveDACs(directory=self.fname, chipname=bname)

        ## Set output files
        cnt_file = 0
        fout = self.fname + '/data_%d.dat' % cnt_file
        f = open(fout, "w")
        self.logging.info("Storing raw hits to %s" % fout)
        f.write("# Settings: Shutter Length = %d s, Threshold = %d dacs" % (shutter_length, thr_dac))
        f.write("# pix_col\tpix_row\ttot\ttot_hd\ttoa(ns)\toa_hd(ns)\ttemp\n")

        ## Set pixel config and readout
        self.mask_bad_pixels()
        self.tpx.pauseReadout()
        self.tpx.setPixelConfig()
        self.tpx.sequentialReadout(tokens=1)
        self.tpx.resetPixels()

        ## Set shutter length
        self.tpx.setShutterLen(int(shutter_length * 1000000))

        ## Sync timing
        self.tpx.resetTimer()
        self.tpx.t0Sync()

        temp = self.tpx.getTpix3Temp()
        time_start = time.time()
        time_elapsed = time.time() - time_start



        # Run
        ## Initialise Variables
        cnt_event = 0
        cnt_total = 0
        tot = []
        pc_cnt = np.zeros((256,256), int)
        itot_cnt = np.zeros((256,256), int)

        ## Open shutter and get frame for every pixel
        for frame in range(nframes):

            ## Open shutter
            self.tpx.openShutter()

            ## Get events
            data = self.tpx.get_frame()

            ## Through away first frame
            if frame == 0:
                self.logging.info("Discarding zero frame ..")

            ## Process the events
            else:
                line = ""
                cnt_event = 0

                for pck in data:
                    if pck.type == 0xA:
                        ftoa_corrected = pck.ftoa+self.tpx.clockPhase(col=pck.col, phase_num=phases) # corrects ftoa for clock phase
                        tot_hd = pck.tot + pck.ftoa/phases - 0.5 # correct tot with ftoa
                        toa_tc = pck.toa * 25 - ftoa_corrected * 25./16
                        line += "%d\t%d\t%d\t%.4f\t%d\t%.4f\t%.1f\n" % (pck.col, pck.row, pck.tot, tot_hd, pck.toa, toa_tc, temp)
                        #tot.append(tot_hd)
                        itot_cnt[pck.col][pck.row] += tot_hd
                        pc_cnt[pck.col][pck.row] += 1
                        cnt_event += 1

                        # elif not (d.col, d.row) in self.bad_pixels: # suppress information about the pixels which are known to be bad/noisy/...
                        #     self.warning_detailed("Unexpected packet %s" % str(d))

                    elif pck.type != 0x7:
                        self.warning_detailed("Unexpected packet %s" % str(pck))

                if cnt_event < 500:
                    f.write(line)
                    f.write("#\n")
                    f.flush()

                cnt_total += cnt_event

                ## Print start info
                if frame < 100:
                   print "Acquisition running ok. %d events in this frame. %d in total in the first %d frames." % (cnt_event, cnt_total, frame)

                ## Print log info
                if frame%1000 == 0:
                    temp = self.tpx.getTpix3Temp()
                    time_log = datetime.datetime.now().strftime('%H:%M:%S')
                    self.logging.info("Frame Nr. %d, Temperature %.1fC, Number of readout pixels %d. [%s]" % (frame, temp, cnt_event, time_log))

                ## Create graphs
                if frame == 100:
                    if w2g:
                        plt.figure(figsize=(12, 9))
                        ax = plt.subplot(111)
                        ax.spines["top"].set_visible(False)
                        ax.spines["right"].set_visible(False)
                        ax.get_xaxis().tick_bottom()
                        ax.get_yaxis().tick_left()
                        plt.xticks(fontsize=14)
                        plt.yticks(fontsize=14)
                        plt.xlabel("Time over Threshold [DACS]", fontsize=16)
                        plt.ylabel("Number of Events [-]", fontsize=16)
                        plt.hist(tot, color="#3F5D7D", bins=1000)
                        plt.savefig(self.fname + "/tot_hist.png", bbox_inches="tight")
                        plt.clf()
                        self.logging.info("Saving graph to file %s" % (self.fname + "/tot_hist.png"))

                ## Change file
                if frame%100000 == 0:
                    f.close()
                    cnt_file += 1
                    fout = self.fname + '/data_%d.dat' % cnt_file
                    f = open(fout, "w")
                    self.logging.info("Storing raw hits to %s" % fout)
                    f.write("# Settings: Shutter Length = %d s, Threshold = %d dacs" % (shutter_length, thr_dac))
                    f.write("# pix_col\tpix_row\ttot\ttot_hd\ttoa(ns)\toa_hd(ns)\ttemp\n")

        self.warning_detailed_summary()
        self.logging.info("Events collected %d" % cnt_total)
        f.close()



        # Finish
        ## Write to file
        if w2f:
            self.save_np_array(pc_cnt, fn = self.fname + '/pc.map')
            self.save_np_array(itot_cnt, fn = self.fname + '/itot.map')

        ## Print graph
        if w2g:
            col = []
            row = []
            val = []
            for i in range(256):
                for j in range(256):
                    col.append(i)
                    row.append(j)
                    val.append(pc_cnt[i][j])

            plt.figure(figsize=(12, 9))
            ax = plt.subplot(111)
            ax.set_xlabel('column', fontsize=22)
            ax.set_ylabel('row', fontsize=22)
            plt.hist2d(col, row, bins=256, range=[[0, 255], [0, 255]], weights=val)
            plt.colorbar()
            plt.savefig(self.fname + "/pc_map.png", bbox_inches="tight")
            plt.clf()
            self.logging.info("Saving graph to file %s" % (self.fname + "/pc_map.png"))
