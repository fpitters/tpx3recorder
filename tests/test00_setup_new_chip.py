# ============================================================================
# File: test00_setup_new_chip.py
# ------------------------------
#
# Notes:
#
# Layout:
#   configure and prepare
#   save and finish
#
# Status:
#   works well
#
# ============================================================================


from tpx3_test import *
from SpidrTpx3_engine import *
from SpidrTpx3 import tpx3packet_hp
from dac_defaults import dac_defaults
import os
import random
import time
import logging
import shutil
import sys




# Main Class
# ============================================================================

class test00_setup_new_chip(tpx3_test):
    """Set up environment for a new assembly."""


    # Execute
    # ---------------------------------

    def _execute(self, **keywords):

        ## Set config directory
        bname = self.tpx.readName()
        fbase = ("config/%s" % bname)
        self.logging.info( "Chip name : %s" % bname)

        ## Create directories
        self.mkdir(self.fname)
        self.mkdir(fbase)

        ## Save DACS
        self.tpx.setDacsDflt()
        self.tpx.saveDACs(directory=self.fname, chipname=bname)
        self.tpx.saveDACs(directory=fbase, chipname=bname)

        ## Test
        self.tpx.loadDACs(directory=fbase, chipname=bname)
