# ============================================================================
# File: test24_tot_toa_calibration_fpipper.py
# ------------------------------
#
# Notes:
#   - Adapted from 'test24_tot_toa_calibration.py'.
#   - Made configuration more obvious
#   - Running in TOA_TOT mode
#   - There is a bug/effect where for (seemingly) random testpulse masks zero
#   testpulses are recieved, for large masks the frequency of this is low, e.g.
#   16x16 spacing at 100 tp with 100 period gives approx. 10 such zero runs
#
# Layout:
#   configure and prepare
#   for each mask:
#       for every tp amp:
#           send 1 digital testpulse -> record toa response for reference
#           send N analog testpulses -> record sum of values + sum of squares for tot and toa response
#   for every pixel:
#       calculate mean and variance -> tot, tot_var, toa, toa_var, pc
#   save and finish
#
# Status:
#   Works well
#
# ============================================================================

from tpx3_test import *
from SpidrTpx3_engine import *
from SpidrTpx3 import tpx3packet_hp
from dac_defaults import dac_defaults
from scipy.optimize import curve_fit
import os
import random
import time
import logging
import shutil
import sys
import numpy as np



# Definitions
# ============================================================================

def linef(x, *p):
    slope, intercept = p
    return  np.array(x) * slope + intercept

def zipdir(fname,path):
    zip = zipfile.ZipFile(fname, 'w')
    print fname, path
    for root, dirs, files in os.walk(path):
        for file in files:
            zip.write(os.path.join(root, file))
    zip.close()

def data_per_mask(tp, N):
    return tp * N * 64 # in bit

def time_per_mask(tp, period, clock):
    return tp * 2 * (period * 64 + 1) / float(clock) # in 1/clock_freq



# Main Class
# ============================================================================

class test05_testpulse_calibration(tpx3_test):
    """TOT and TOA calibration with testpulses in data driven acquisition. Uses TOA_TOT mode.

    Parameters:
    polarity ... polarity flag, holes = 0, electrons = 1
    testpulses ... testpulse flag, equalise at baseline = 0, with testpulse = 1

    ik_dac ... discharge current dac
    period_dac ... sets duration between two testpulses, 8bit dac

    thr_est ... threshold estimation used for failed pulse counting
    capacitance ... assumed value of capacitance in [e-/mV], around 3.2 fF

    npulses ... number of testpulses sent per shutter
    spacing_x ... spacing within one col
    spacing_y ... spacing between col
    """

    def _execute(self, **keywords):
        self.tpx.resetPixels()
        self.tpx.shutterOff()
        self.tpx.setDacsDflt()



        # Set up
        # ---------------------------------

        # User Configuration
        polarity = 1            # polarity flag, holes = 0, electrons = 1
        testpulses = 0          # testpulse flag, equalise at baseline = 0, with testpulse = 1

        ik_dac = 10             # discharge current dac
        period_dac = 100        # sets duration between two testpulses, 8bit dac
                                # must be high enough to allow the capacitor to discharge
                                # and keep within the max bandwidth

        thr_est = 1000          # threshold estimation used for failed pulse counting
        capacitance = 20.2      # assumed value of capacitance in [e-/mV], around 3.2 fF

        npulses = 100           # number of testpulses sent per shutter
        spacing_x = 16          # spacing within one col
        spacing_y = 16          # spacing between col


        ## Energy List
        START_ENERGY_fs = 200
        STOP_ENERGY_fs = 1600
        STEP_ENERGY_fs = 100

        START_ENERGY_fs1 = 1700
        STOP_ENERGY_fs1 = 3600
        STEP_ENERGY_fs1 = 500

        START_ENERGY = 4000
        STOP_ENERGY = 15000
        STEP_ENERGY = 1000

        energyList = []
        for Energy_e in range(START_ENERGY_fs, STOP_ENERGY_fs, STEP_ENERGY_fs):
            energyList.append(Energy_e)
        for Energy_e in range(START_ENERGY_fs1, STOP_ENERGY_fs1, STEP_ENERGY_fs1):
            energyList.append(Energy_e)
        for Energy_e in range(START_ENERGY, STOP_ENERGY, STEP_ENERGY):
            energyList.append(Energy_e)

        self.mkdir(self.fname)
        f = open(self.fname + '/Energy_table.dat', "w")

        ## Clock
        clk_period = 40

        if clk_period == 20:
            phase_shift = TPX3_PHASESHIFT_DIV_16
            phases = 16.0
        elif clk_period == 40:
            phase_shift = TPX3_PHASESHIFT_DIV_8
            phases = 16.0
        elif clk_period == 80:
            phase_shift = TPX3_PHASESHIFT_DIV_4
            phases = 8.0
        elif clk_period == 160:
            phase_shift = TPX3_PHASESHIFT_DIV_2
            phases = 4.0


        # Preperation
        ## Set config directory
        bname = self.tpx.readName()
        fbase = "config/%s/" % (bname)
        self.logging.info("Chip name    : %s" % bname)
        self.tpx.loadDACs(directory = fbase, chipname = bname)

        ## Set operating parameters
        self.tpx.setDecodersEna(True)
        self.tpx.setOutputMask(0xFF)
        self.tpx.setPllConfig( (TPX3_PLL_RUN | TPX3_VCNTRL_PLL | TPX3_DUALEDGE_CLK | phase_shift | TPX3_PHASESHIFT_NR_16 | 0x14<<TPX3_PLLOUT_CONFIG_SHIFT) )
        self.tpx.setSlvsConfig(TPX3_SLVS_TERMINATION)
        if not polarity:
            self.tpx.setGenConfig( TPX3_ACQMODE_TOA_TOT | TPX3_FASTLO_ENA | TPX3_GRAYCOUNT_ENA | TPX3_POLARITY_EMIN )
        else:
            self.tpx.setGenConfig( TPX3_ACQMODE_TOA_TOT | TPX3_FASTLO_ENA | TPX3_GRAYCOUNT_ENA )

        ## Set operating conditions
        self.logging.info("Optimization of DC operating point")
        self.tpx.setDac(TPX3_IBIAS_IKRUM, ik_dac)
        # self.tpx.setDac(TPX3_IBIAS_DISCS1_ON, 100)
        # self.tpx.setDac(TPX3_IBIAS_DISCS2_ON, 128)
        # self.tpx.setDac(TPX3_IBIAS_PREAMP_ON, 128)
        # self.tpx.setDac(TPX3_IBIAS_PIXELDAC, 128)
        # self.tpx.setDac(TPX3_VFBK, 164)

        ## Reset pixels
        self.tpx.resetPixelConfig()

        ## Load equalisation codes and mask in Xavi's format
        if 0:
            codes_eq = np.loadtxt(fbase + "/eq_codes_finestep_%s_f.dat" % bname, int)
            mask = np.loadtxt(fbase + "/eq_mask_finestep_%s_f.dat" % bname, int)
            self.logging.info( "Mask and trim dacs loaded from Xavi's format.")

        ## Load equalisation codes and mask in LHCb format
        if 1:
            bfullname = "%s%04i_%s%02i" % ("W", int(bname.split("_")[0].split("W")[1]), bname.split("_")[1][0], int(bname.split("_")[1][1:]))
            trim = np.loadtxt(fbase + "/%s_trimdacs.txt" % (bfullname))
            codes_eq = np.zeros((256, 256), int)
            mask = np.zeros((256, 256), int)
            for x in range(256):
                for y in range(256):
                    codes_eq[y][x] = trim[x*256+y][2]
                    mask[y][x] = trim[x*256+y][3]
            self.logging.info( "Mask and trim dacs loaded from LHCb format.")

        ## Set ctpr bits
        self.tpx.setCtprBits(0)
        self.tpx.setCtpr()
        self.tpx.setOutputMask(0xFF)

        self.mkdir(self.fname)
        self.tpx.setThreshold(thr_dac)
        self.tpx.saveDACs(directory = self.fname, chipname = bname)

        ## Calibrate TP linearity
        if testpulses:
            self.tpx.setTpPeriodPhase(period_dac, 4) # (period, phase)
            self.tpx.setTpNumber(npulses)
            pulse_period = 2 * (64 * period_dac + 1) # in clock cycles
            shutter_length = int(((pulse_period * npulses)/clk_period) + 100) # in ns

            self.tpx.setShutterLen(shutter_length)
            self.logging.info("Shutter length %d us" % shutter_length)
            self.logging.info("Pulse period %d us" % int(pulse_period * 25/1000))
            self.logging.info("Threshold at %d, discharge current at %d." % (thr_dac, self.tpx.getDac(TPX3_IBIAS_IKRUM)))
            self.logging.info("Number of testpulses: %d, Spacing: %dx%d" % (npulses, spacing_x, spacing_y))
            self.logging.info("Chip Temperature is %.1f degrees" % self.tpx.getTpix3Temp())

            self.tpx.setDac(TPX3_VTP_COARSE, 64)
            self.tpx.setSenseDac(TPX3_VTP_COARSE)
            coarse = self.tpx.get_adc(64)
            self.tpx.setSenseDac(TPX3_VTP_FINE)
            code_x = []
            electrons_y = []
            for code in range(100, 440, 2):
                self.tpx.setDac(TPX3_VTP_FINE, code) # (0e-) slope 44.5e/LSB -> (112=1000e-)  (135=2000e-)
                time.sleep(0.001)
                fine = self.tpx.get_adc(64)
                electrons = 1000.0 * capacitance * (fine-coarse)
                code_x.append(code)
                electrons_y.append(electrons)
                if code == 100 or code == 438:
                    print code, electrons

            p0 = [50, -6000]
            coeff, var_matrix = curve_fit(linef, code_x, electrons_y, p0=p0)
            print coeff



        # Run
        # ---------------------------------

        # Calibration
        ## Initialise
        steps_energy = len(energyList)
        pc_cnt = np.zeros((steps_energy, 256, 256), int)
        tot_cnt = np.zeros((steps_energy, 256, 256), float)
        toa_cnt = np.zeros((steps_energy, 256, 256), float)
        tot_var = np.zeros((steps_energy, 256, 256), float)
        toa_var = np.zeros((steps_energy, 256, 256), float)
        toa_ref = np.zeros((steps_energy, 256, 256), float)
        cnt_fail = 0
        cnt_low = 0

        ## Set mask for testpulses
        seq_total = spacing_x * spacing_y

        for seq in range(seq_total):
            good_pixels = 0

            ## Grid of 4x4
            if 1:
                self.tpx.setPixelMask(ALL_PIXELS, ALL_PIXELS, 1)
                self.tpx.setCtprBits(0)
                for y in range(256):
                    for x in range(256):
                        self.tpx.setPixelTestEna(y, x, testbit=False)
                        self.tpx.setPixelThreshold(y, x, codes_eq[x][y])
                        if x%spacing_x == (int)(seq/spacing_y) and y%(spacing_y) == seq%(spacing_y):
                            self.tpx.setPixelMask(y, x, mask[x][y])
                            self.tpx.setPixelTestEna(y, x, testbit=True)
                            self.tpx.setCtprBit(y, 1)
                            good_pixels += 1
                self.tpx.pauseReadout()
                self.tpx.setPixelConfig()
                self.tpx.setCtpr()

            ## Injecting full superpixel
            if 0:
                self.tpx.setPixelMask(ALL_PIXELS, ALL_PIXELS, 1)
                self.tpx.setPixelTestEna(ALL_PIXELS, ALL_PIXELS, testbit=False)
                self.tpx.setCtprBits(0)
                for y in range(256):
                    for x in range(256):
                        self.tpx.setPixelThreshold(y, x, codes_eq[x][y])
                for x_vco in range(64):
                    for y_vco in range(128):
                        if x_vco%spacing_x == (int)(seq/spacing_y) and y_vco%(spacing_y) == seq%(spacing_y):
                            for xx in range(4):
                                for yy in range(2):
                                   x = x_vco*4 + xx
                                   y = y_vco*2 + yy
                        # if x == 128 and y == 128:
                                   self.tpx.setPixelMask(y, x, mask[x][y])
                                   self.tpx.setPixelTestEna(y, x, testbit=True)
                                   self.tpx.setCtprBit(y, 1)
                                   good_pixels += 1
                                   # print "(%d, %d)" % (y, x)
                self.tpx.pauseReadout()
                self.tpx.setPixelConfig()
                self.tpx.setCtpr()

            ## Prepare timing and datadriven readout
            self.tpx.resetPixels()
            self.tpx.datadrivenReadout()

            self.tpx.resetTimer()
            self.tpx.t0Sync()

            ## Loop through all testpulse amplitudes
            ## (Note that due to an effect/bug where the loop fails for all pixels in one energy step, I switched from a for loop to a while loop and repeat the step if the event counter is 0) <---- switched back to for loop and run at large spacings
            # i = 0
            # while i < steps_energy:
            for i in range(steps_energy):
                Energy_e = energyList[i]
                pos = int((Energy_e - START_ENERGY)/STEP_ENERGY)

                ## Correct testpulse amplitude
                if testpulses:
                    best_vtpfine_code = (int)((Energy_e-coeff[1])/coeff[0])
                    self.tpx.setDac(TPX3_VTP_FINE, best_vtpfine_code)
                    self.tpx.setSenseDac(TPX3_VTP_FINE)
                    time.sleep(0.001)
                    fine = self.tpx.get_adc(32)
                    volts = 1000.0 * (fine-coarse)
                    electrons = 1000.0 * capacitance * (fine-coarse)
                    #self.tpx.setGenConfig(genConfig_register)

                ## Write header and flush
                if seq == 0:
                    f.write("%d\t%d\t%d\t%.2f\n" % (Energy_e, best_vtpfine_code, electrons, volts))
                    f.flush()

                temp = self.tpx.getTpix3Temp()

                for frame in (1, 2):
                    ## Digital reference
                    if frame == 1:
                        genConfig_register = TPX3_ACQMODE_TOA_TOT | TPX3_TESTPULSE_ENA | TPX3_SELECTTP_DIGITAL | TPX3_GRAYCOUNT_ENA | TPX3_FASTLO_ENA
                        if not polarity: genConfig_register |= TPX3_POLARITY_EMIN
                        self.tpx.setTpNumber(1)
                        shutter_length = int(((pulse_period*1)/clk_period) + 100)
                        self.tpx.setShutterLen(shutter_length)
                    ## Analog measurement
                    else:
                        genConfig_register = TPX3_ACQMODE_TOA_TOT | TPX3_TESTPULSE_ENA | TPX3_GRAYCOUNT_ENA | TPX3_FASTLO_ENA
                        if not polarity: genConfig_register |= TPX3_POLARITY_EMIN
                        self.tpx.setTpNumber(npulses)
                        shutter_length = int(((pulse_period*npulses)/clk_period) + 100)
                        self.tpx.setShutterLen(shutter_length)

                    shutter_start = 0
                    self.tpx.setGenConfig(genConfig_register)
                    self.tpx.openShutter(sleep=False)
                    shutter_start = self.tpx.getShutterStart()
                    shutter_start = self.tpx.getShutterStart()
                    shutter_start = self.tpx.getShutterStart()
                    finish = 0
                    cnt_evnt = 0

                    while not finish:
                        wait_data = self.tpx.daq.getSample(1024*4, 100)
                        if wait_data:
                            raw_pck = self.tpx.daq.nextPacket()
                            while (raw_pck):
                                pck = tpx3packet_hp(raw_pck)
                                if pck.isData():
                                    ftoa_corrected = pck.ftoa+self.tpx.clockPhase(col=pck.col, phase_num=16)
                                    hitTime = (pck.toa-shutter_start) & 0x0003fff
                                    hitTimef = float(hitTime * 25 - float(ftoa_corrected) * 25/16)

                                    ## Hit time reference
                                    if frame == 1:
                                        if polarity:
                                            toa_ref[i][pck.col][pck.row] = hitTimef
                                        else:
                                            toa_ref[i][pck.col][pck.row] = hitTimef + (pulse_period/2) * 25

                                    else:
                                        time_adj = 25 * ((pulse_period*(pc_cnt[i][pck.col][pck.row])) % 16384)
                                        timewalk = hitTimef - ((toa_ref[i][pck.col][pck.row] + time_adj) % (16384*25))

                                        ## Accept only events in the correct time frame to eliminate overshot events
                                        if timewalk < 300 and timewalk > 0 and 1:
                                            cnt_evnt += 1
                                            tot_hd = pck.tot + pck.ftoa/phases - 0.5
                                            tot_hd_temp = tot_hd * (temp * 0.009 + 0.445) # ignore this one, better to keep T stable by cooling

                                            tot_cnt[i][pck.col][pck.row] += tot_hd
                                            toa_cnt[i][pck.col][pck.row] += timewalk
                                            pc_cnt[i][pck.col][pck.row] += 1
                                            tot_var[i][pck.col][pck.row] += tot_hd**2
                                            toa_var[i][pck.col][pck.row] += timewalk**2


                                else:
                                    if pck.isEoR():
                                        finish = 1
                                raw_pck = self.tpx.daq.nextPacket()

                self.logging.info("  seq %0d/%d %d %.1f C %d Target=%d TPX3_VTP_FINE code=%d Measured=%.1f %.2f%%" % (seq+1, seq_total, i, temp, thr_dac, Energy_e, best_vtpfine_code, electrons, cnt_evnt*100./(npulses*good_pixels)))

                if cnt_evnt == 0 and energyList[i] > thr_est:
                    cnt_fail += 1

                if (cnt_evnt*100./(npulses*good_pixels)) < 95 and energyList[i] > (thr_est + 200):
                    cnt_low += 1

            print cnt_fail, cnt_low

        self.logging.info("In total %d steps recieved zero testpulses." % (cnt_fail))
        self.logging.info("In total %d steps recieved less than 95%% of testpulses." % (cnt_low))

        ## Get variance
        for i in range(steps_energy):
            for x in range(256):
                for y in range(256):
                    if pc_cnt[i][x][y] > 1: # ignore pc = 1 cases, they have var = 0
                        tot_var[i][x][y] = (pc_cnt[i][x][y] * tot_var[i][x][y] - tot_cnt[i][x][y]**2) / (float(pc_cnt[i][x][y])**2)
                        toa_var[i][x][y] = (pc_cnt[i][x][y] * toa_var[i][x][y] - toa_cnt[i][x][y]**2) / (float(pc_cnt[i][x][y])**2)

        ## Get mean
        for i in range(steps_energy):
            for x in range(256):
                for y in range(256):
                    if pc_cnt[i][x][y] > 0:
                        tot_cnt[i][x][y] = tot_cnt[i][x][y] / pc_cnt[i][x][y]
                        toa_cnt[i][x][y] = toa_cnt[i][x][y] / pc_cnt[i][x][y]

        # Finish
        ## Save results in matrix style
        for i in range(steps_energy):
            self.save_np_array(pc_cnt[i], fn = self.fname + '/PC_%d.map' % energyList[i], fmt="%d")
            self.save_np_array(tot_cnt[i], fn = self.fname + '/TOT_%d.map' % energyList[i], fmt="%.5f")
            self.save_np_array(tot_var[i], fn = self.fname + '/TOT_VAR_%d.map' % energyList[i], fmt="%.5f")
            self.save_np_array(toa_cnt[i], fn = self.fname + '/TOA_%d.map' % energyList[i], fmt="%.5f")
            self.save_np_array(toa_var[i], fn = self.fname + '/TOA_VAR_%d.map' % energyList[i], fmt="%.5f")
            self.save_np_array(toa_ref[i], fn = self.fname + '/TOA_REF_%d.map' % energyList[i], fmt="%.5f")

        ## Save results in list style
        for i in range(steps_energy):
            fout = open(self.fname + '/tp_dat_%d.dat' % energyList[i], "w")
            fout.write("#col\trow\tpc\ttot\ttot_var\ttoa\ttoa_var\ttoa_ref\n")
            for col in range(0, 256):
                for row in range(0, 256):
                    fout.write("%d\t%d\t%d\t%10.5f\t%10.5f\t%10.5f\t%10.5f\t%10.5f\t\n" % (col, row, pc_cnt[i][col][row], tot_cnt[i][col][row],
                        tot_var[i][col][row], toa_cnt[i][col][row], toa_var[i][col][row], toa_ref[i][col][row]))
            fout.close()

        f.close()
