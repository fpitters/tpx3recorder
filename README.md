# tpx3recorder

This is a set of scripts to be used in combination with the SPIDR framework to operate the Timepix3 ASIC. You will need to run the full [SPIDR repository](https://gitlab.cern.ch/CLICdp/DAQs/SPIDR.git) to use these scripts. THEY WILL NOT WORK STANDALONE!


## Installation

Copy the scripts into the python/tests folder of the SPIDR framework.
```
git clone https://gitlab.cern.ch/CLICdp/DAQs/SPIDR.git
git clone https://gitlab.com/fpitters/tpx3recorder.git
cp tpx3recorder/test_* SPIDR/software/python/tests
```


## Usage



### General Information on the Python Scripts

The framework allows the use of a set of python script to communicate with TPX3, characterise the chip or take data. The scripts can be found in the subfolder 'tests/'. Results are saved to 'logs/'.

To execute a script, type
   * ``` ./tpx3_tests.py [target_directory] [script_name] ```

It will save all results to `[target_directory]` in 'logs/'. If the folder doesn't exist yet, it will be created. For each execution, a subfolder will be created named `[number]_[timestamp]` with consecutive numbering.

To list all available scripts (i.e. all scripts registered in the 'tests/' module)
   * ``` ./tpx3_tests.py [target_directory] -l ```

To add a new script, create a new file in the 'tests/' folder. In the file, create a new class that will act as the script. The init file automatically scans the folder and registered all available classes.

If things don't work or certain settings can not be found, a list of interesting files is
```
./tpx3_tests.py
./tests/tpx3_test.py
./SpidrTpx3/tpx3.py
```

Some debugging notes
   * Is the IP address of the board correct? You manually can set it with the '-i [address]' flag.
   * Does the device id setting correspond to the FMC connector that is used? If there is a error message 'links unlocked', this setting is likely to be wrong. You manually can set it manually with the '-x [id]' flag.
   * In the vertex lab PC, only one of the 10 Gbit ethernet ports on pcvertextb works.


### Setting up a new Assembly

##### Step 0: Set up directory and basic files for new chip
First you need to create the config files for the chip id in the 'config/' folder. Script 00 creates the config directory for a new assembly and saves the default dac settings into a file to be read by further tests.

Example:
   * ``` /tpx3_tests.py W19_C7 test00_setup_new_chip ```



##### Step 1: Equalisation
Next you want to equalise the threshold of all pixels by finding a set of trim dacs that minimise the dispersion accross the pixel matrix. Script 01 equalises at the noise edge, i.e. it scans the threshold over the noise floor and records the threshold DAC at which it first records more than X noise hits in a certain time window. Other application might prefer to equalise at the noise peak (the pedestal mean), the TOT threshold or a certain x-ray energy. The script also masks pixels that are too noisy. It returns a file holding trim dacs and mask bits as well as a file holding all the dac settings used in the run. If the update flag is set within the script, the files are automatically copied to the 'config/' folder.

Example:
   * ``` ./tpx3_tests.py W19_C7 test01_threshold_equalisation ```

Notes:
   * Equalising at the noise edge gives a larger spread of the baseline. If one takes the total system noise to be sqrt(noise_mean^2 + baseline_rms^2), the resulting values will be larger than when equalising at the baseline.


##### Step 2: Noise scan
Next you want to measure the noise. Script 02 scans the threshold over the noise floor and records the number of noise pulses in a certain time window (e.g. 100 us) for that threshold. Returns mean and rms of the noise floor.

Example:
   * ``` ./tpx3_tests.py W19_C7 test02_noise_scan ```

Notes:
   * The use of the global gray counter increases the noise. The noise scan is done in PC & iTOT mode that does not utilise the gary counter. But the TOA & TOT mode does. Therefore, this script activates the gray counter to get a realistic measurement of the noise for later measurements. To deactivate it, look for the line `self.tpx.setGenConfig( TPX3_ACQMODE_EVT_ITOT | TPX3_SELECTTP_DIGITAL | TPX3_GRAYCOUNT_ENA )` and remove the 'TPX3_GRAYCOUNT_ENA' argument.



##### Step 3: Gain Measurement
You might also be interested in the gain of the chip. Script 03 measures the gain for all diagonal pixels. First, a noise scan is performed to extract the pedestal mean. Then, s-curves are recorded for multiple testpulse injection amplitudes. A linear regression is performed to get the gain in mV/LSB. To convert volts to electrons, an assumption about the input capacitor has to be made. The script returns the baseline, gain and recommended threshold setting.

Example:
   * ``` ./tpx3_tests.py W19_C7 test03_gain_measurement ```


##### Step 4: Testpulse Calibration
Script 05 injects testpulses with different amplitudes levels and records the TOT and TOA response. The resulting data can be fitted to extract a, b, c, t constants for TOT and c, t, d for TOA. calibration is done pixel by pixel.

Example:
   * ``` ./tpx3_tests.py W19_C7 test05_testpulse_calibration ```

Note:
   * The testpulse injection is done by applying a voltage step pulse over a input capacitor. To obtain a charge scale, one has to make assumptions on the value of that capacitor. It is around 3 fF. However, comparision with source data has shown that the a simple conversion Q = CV is not completely correct. The correct conversion is more likely to be Q = CV + const, so it requires atleast two source data points. However, this is only necessary if high precision charge information is essential for the application. For lower precision, adapting a C' value so that Q = CV + const ~ C'V yields the correct charge value in the region of interest will be sufficient.


##### Step 5: Acquisition

Example:
   * ``` ./tpx3_tests.py W19_C7 test08_framebased ```
   * ``` ./tpx3_tests.py W19_C7 test08_datadriven ```

Description:
   * This script records data for either a certain amount of time or until a certain amount of events are registered. See the scripts for details.
  
  

